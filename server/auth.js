// var bycrypt = require('bcryptjs');
// var AuthProvider = require("./database").AuthProvider;

// var config = require("./config");
// var LocalStrategy = require("passport-local").Strategy;
// var User = require('./db').User;

var LocalStrategy = require("passport-local").Strategy;
// var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
// var FacebookStrategy = require("passport-facebook").Strategy
// var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
// var WechatStrategy = require("passport-wechat").Strategy
// var TwitterStrategy  = require('passport-twitter').Strategy;
var bcrypt = require('bcryptjs');

// var User = require("./database").User;
// var AuthProvider = require("./database").AuthProvider;
var config = require("./config");

var _user;
function User(app) {
    if (!_user) {
        _user = app.locals.db.collection(config.USER_DB);
    }
    return _user;
}


module.exports = (app, passport) => {
    // function authenticate(username, password, done) {
    //     console.log("in authenticate");
    //     var userCol = app.locals.db.collection(config.USER_DB);
    //     userCol.insertOne({hello:"hello"}).then((result) => {

    //         console.log(result);
    //     })
    //     userCol.findOne({hello:"hello"}).then((result) => {

    //         console.log(result);
    //     })
    //     console.log();
    //     if (userCol) {
    //         console.log("userCol not empty");
    //     } else {
    //         console.log("userCol is empty");
    //     }
    //     // return done(err, false);
    //     // User(app).findOne({username:username})
    //     // .then((result)=>{
    //     //     console.log(result); 
    //     // })
    // }
    // passport.use(new LocalStrategy({
    //     usernameField: "email",
    //     passwordField: "password"
    // }, authenticate));
    // passport.serializeUser(function (username, done) {
    //     console.log("in serialize")
    //     done(null, username);
    // });
    // passport.deserializeUser(function (id, done) {
    //     console.log("in serialize")
    //     done(null, userObject);
    // });
    function authenticate(username, password, done) {
        console.log("in authenticate")
        // User.findOne({
        //     where: {
        //         email: username
        //     }
        // User(app).insertOne({test:"test"}).then((results)=>{

        //     console.log("then");
        // })
        // console.log(username)
        // console.log(password)
        User(app).findOne({
            username: username
        }).then(function (result) {
            // console.log("compared")
            // console.log(result)
            if (!result) {
                // console.log("not result");
                return done(null, false);
            } else {
                if (bcrypt.compareSync(password, result.password)) {
                // if (password == result.password) {
                    // console.log("hashed password");
                    var whereClause = {};
                    whereClause.username = username;
                    // User(app)
                    //     .updateOne({ reset_password_token: null },
                    //     { where: whereClause });
                    User(app).updateOne(whereClause,
                    {$set:{ reset_password_token: null }})
                    // console.log("passing user info to passport")
                    var dataToStore = {
                        username:result.username,
                        _id:result._id,
                        privilege:result.privilege,
                    }
                    // return done(null, result);
                    return done(null,dataToStore)
                } else {
                    // console.log("updateOne failed")
                    return done(null, false);
                }
            }
        }).catch(function (err) {
            console.log("error in authenticate")
            console.log(err);
            return done(err, false);
        });
    }

    // function verifyCallback(accessToken, refreshToken, profile, done) {
    //     console.log(profile);
    //     if(profile.provider === 'google' || profile.provider === 'facebook'|| profile.provider === 'linkedin'){
    //         id = profile.id;
    //         email = profile.emails[0].value;
    //         displayName = profile.displayName;
    //         provider_type = profile.provider;
    //         User.findOrCreate({where: {email: email}, defaults: {username: email , email: email, password: null, name: displayName}})
    //             .spread(function(user, created) {
    //                 console.log(user.get({
    //                     plain: true
    //                 }));
    //                 console.log(created);
    //                 AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
    //                     defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
    //                     .spread(function(provider, created) {
    //                         console.log(provider.get({
    //                             plain: true
    //                         }));
    //                         console.log(created);
    //                     });
    //                 done(null, user);
    //             });
    //     }else if(profile.provider === 'twitter'){
    //         id = profile.id;
    //         twitterUsername = profile.username;
    //         displayName = profile.displayName;
    //         provider_type = profile.provider;
    //         User.findOrCreate({where: {email: twitterUsername}, defaults: {username: twitterUsername, email: twitterUsername, password: null , name: displayName}})
    //             .spread(function(user, created) {
    //                 console.log(user.get({
    //                     plain: true
    //                 }));
    //                 console.log(created);
    //                 AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
    //                     defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
    //                     .spread(function(provider, created) {
    //                         console.log(provider.get({
    //                             plain: true
    //                         }));
    //                         console.log(created);
    //                     });
    //                 done(null, user);
    //             });
    //     }else{
    //         done(null, false);
    //     }
    // }

    // passport.use(new LinkedInStrategy({
    //     clientID: config.linkedin_key,
    //     clientSecret: config.Linkedin_secret,
    //     callbackURL: config.Linkedin_callback_url,
    //     scope: ['r_emailaddress', 'r_basicprofile'],
    // }, verifyCallback));

    // passport.use(new WechatStrategy({
    //         appID: config.Wechat_AppId,
    //         name: config.Wechat_Name,
    //         appSecret: config.Wechat_AppSecret,
    //         client: "web",
    //         callbackURL: config.Wechat_Callback_Url,
    //         scope: "snsapi_userinfo",
    //         state:""
    //     },
    //     function(accessToken, refreshToken, profile, done) {
    //         return done(err,profile);
    //     }
    // ));

    // passport.use(new TwitterStrategy({
    //     consumerKey: config.Twitter_key,
    //     consumerSecret: config.Twitter_secret,
    //     callbackURL: config.Twitter_callback_url
    // }, verifyCallback))

    // passport.use(new GoogleStrategy({
    //     clientID: config.GooglePlus_key,
    //     clientSecret: config.GooglePlus_secret,
    //     callbackURL: config.GooglePlus_callback_url
    // }, verifyCallback))

    // passport.use(new FacebookStrategy({
    //     clientID: config.Facebook_key,
    //     clientSecret: config.Facebook_secret,
    //     callbackURL: config.Facebook_callback_url,
    //     profileFields: ['id', 'displayName', 'photos', 'email']
    // }, verifyCallback))

    passport.use(new LocalStrategy({
        usernameField: "username",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        // console.info("serial to session");
        // console.log(user);
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        // console.log("to deserialize");
        User(app).findOne({
                username: user.username
        }).then(function (result) {
            // console.log("found user")
            if (result) {
                done(null, user);
            }
        }).catch(function (err) {
            // console.log("lost user")
            done(err, user);
        });
    });
}