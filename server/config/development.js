'use strict';

module.exports = {
  domain_name: "http://localhost:3000",
  PORT: 3000,
  DATABASE_URI: "mongodb://localhost:27017/mydb",
  // DATABASE_URI:"mongodb://cluster1:1234@cluster1-shard-00-00-mgbtk.mongodb.net:27017,cluster1-shard-00-01-mgbtk.mongodb.net:27017,cluster1-shard-00-02-mgbtk.mongodb.net:27017/mydb?ssl=true&replicaSet=Cluster1-shard-0&authSource=admin",
  USER_DB: "user",
  COMPANY_DB: "company",
  PS_DB: "ps",
  COUNTRY_DB:"country",
  USER_SECURE: "userAccountSecure",
  MAILGUN_API_KEY: "key-cedc75b4a039095152611990d365ae78",
  // seedClean: false,
  seedClean: true,
  // MYSQL_USERNAME: 'root',
  // MYSQL_PASSWORD: 'root',
  // MYSQL_HOSTNAME: 'localhost',
  // MYSQL_PORT: 3306,
  // MYSQL_LOGGING: console.log,
  seedUser: true,
  // seedUser:false,
  seedAdmin: true,
  // version: '1.0.0'
};