// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");
var flash = require('connect-flash');

require('./auth.js')(app, passport);
// CONSTANTS --------------------------------------------------------------------------------------
const config = require('./config');

// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || config.PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
// CLIENT FOLDER is the public directory
const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

// Creates an instance of express called app
var app = express();

// Load Database Models
// const  = require('./db')();
// Attempt Mongo Connection
require('./db')(app);

app.use(flash());
app.use(cookieParser());

// Populates req.body with information submitted through the registration form.
// Default $http content type is application/json so we use json as the parser type
// For content type application/x-www-form-urlencoded,
// use: app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Initialize session
app.use(session({
	secret: "nodematch",
	resave: false,
	saveUninitialized: true,
}))

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());



/********************************************* */
//Database variable
var db;

// var MongoClient = require('mongodb').MongoClient;
// var url = "mongodb://localhost:27017/mydb";

// // SERVER / PORT SETUP ----------------------------------------------------------------------------
// // Server starts and listens on NODE_PORT
// MongoClient.connect(url, function (err, database) {
// 	if (err) throw err;
// 	db = database;
// 	console.log("Mongo started")

// });

// app.use("/example", (req, res) => {
// 	console.log("enter example")
// 	var col = db.collection("customers");
// 	var myobj = {
// 		name: req.query.name,
// 	}
// 	col.insertOne(myobj, function (err, res) {
// 		if (err) throw err;
// 		console.log("Inserted");
// 	})
// })

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// ROUTE HANDLERS ---------------------------------------------------------------------------------
// Load Routes handlers
const routes = require('./routes')(app, passport);
require('./auth.js')(app, passport);

// ERROR HANDLING ---------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
	res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
	console.log(err);
	res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
});

app.listen(NODE_PORT, function () {
	console.log("Server running at http://localhost:" + NODE_PORT);
});

//we want to connect to database before we start our app
// var databaseManager = require('./db');
// databaseManager.connect(app)
// 	.then((db) => {
// 		databaseManager.initialize(db);
// 		//Need to the database to be ready before we put our routes
// 		console.log(databaseManager.collections);
// 		require("./routes")(app, passport,databaseManager.collections);
// 		app.listen(NODE_PORT, function () {
// 			console.log("Server running at http://localhost:" + NODE_PORT);
// 		});
// 	}).catch((err) => {
// 		console.log(err);
// 	})