// Handles API routes

// var UserController = require("./api/user/user.controller");
// var PostController = require("./api/post/post.controller");
// var CommentController = require("./api/comment/comment.controller");
// var AWSController = require("./api/aws/aws.controller");
// var express = require("express");
// var config = require("./config");

// const API_POSTS_URI = "/api/posts";
// const API_USERS_URI = "/api/users";
// const API_COMMENTS_URI = "/api/comments";
// const API_AWS_URI = "/api/aws";
const HOME_PAGE = "/#!/home.html";
const SIGNIN_PAGE = "/#!/login.html";

module.exports = function (app, passport) {
    var UserController = require('./api/user.controller')
    var PSController = require('./api/ps.controller')
    var CompanyController = require('./api/company.controller')
    var CountryController = require('./api/country.controller')
    app.get("/user/auth", (req, res) => {
        // console.log(req.url);
        // console.log(req.session.passport.user)
        // console.log(req.user);
        if (req.user) {
            res.status(200).json(req.user);
        } else {
            res.status(401).json({
                message: "Unauthorized"
            })
        }
    });

    // app.post("/login",function(req,res){
    //   console.log("login")
    //   res.status(200).json({ok:"ok"})
    // })
    // console.log(passport);

    // login debug (DO NOT DELETE)
    //   app.post('/user/login', function (req, res, next) {
    //     console.log(req.url);
    //     passport.authenticate('local', function (err, user, info) {
    //         console.log(req.body);
    //       console.log("authenticate");
    //       console.log(err);
    //       console.log(user);
    //       console.log(info);
    //     })(req, res, next);
    //   });
    app.post("/user/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        // failureRedirect: "/",
        // failureRedirect: "/",
        // failureRedirect: SIGNIN_PAGE,
        failureRedirect: {message:"Unauthorized"},//SIGNIN_PAGE,
        failureFlash: true
    }));

    // Posts API
    // app.get(API_POSTS_URI, isAuthenticated, PostController.list);
    // app.get(API_POSTS_URI + '/image/:url', isAuthenticated, PostController.showImage);
    // app.get(API_POSTS_URI + '/me', isAuthenticated, PostController.me);
    // app.post(API_POSTS_URI, isAuthenticated, PostController.create);
    // app.get(API_POSTS_URI + '/:id', isAuthenticated, PostController.get);
    // app.post(API_POSTS_URI + '/:id', isAuthenticated, PostController.update);
    // app.delete(API_POSTS_URI + '/:id', isAuthenticated, PostController.remove);
    // app.post(API_POSTS_URI + '/:id/like', isAuthenticated, PostController.likePost);
    // app.get(API_POSTS_URI + '/:postId/comments', isAuthenticated, CommentController.byPosts);

    // // Users API
    // app.get(API_USERS_URI, isAuthenticated, UserController.list);
    // app.post(API_USERS_URI, isAuthenticated, UserController.create);
    // app.get(API_USERS_URI+ '/:id', isAuthenticated, UserController.get);
    // app.get(API_USERS_URI + '/:id/posts', isAuthenticated, PostController.listByUser);
    // app.post(API_USERS_URI + '/:id', isAuthenticated, UserController.update);
    // app.delete(API_USERS_URI + '/:id', isAuthenticated, UserController.remove);
    // app.get("/api/user/view-profile", isAuthenticated, UserController.profile);
    // app.get("/api/user/social/profiles", isAuthenticated, UserController.profiles);

    // // Comments API
    // app.post(API_COMMENTS_URI, isAuthenticated, CommentController.create);
    // app.delete(API_COMMENTS_URI + '/:id', isAuthenticated, CommentController.remove);

    // app.get("/protected/", isAuthenticated, function(req, res){
    //     if(req.user == null){
    //         res.redirect(SIGNIN_PAGE);
    //     }
    // })

    // // AWS policy API
    // app.get(API_AWS_URI + '/createS3Policy', isAuthenticated, PostController.list);
    // app.post(API_AWS_URI + '/s3-policy', isAuthenticated, AWSController.getSignedPolicy);

    // // app.use(express.static(__dirname + "/../client/"));
    // app.use(express.static(__dirname + "/../client/"));
    // app.use(express.static(__dirname ));

    // app.post("/change-password", isAuthenticated, UserController.changePasswd);

    // app.get("/api/user/get-profile-token", UserController.profileToken);
    // app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    //User api
    app.post('/user/signUp', UserController.register);
    app.get('/admin/users', isAuthenticated, UserController.getUserList)
    app.get('/user/profile/contact/:id', isAuthenticated, UserController.getUser)
    app.put('/user/profile/contact/:id', isAuthenticated, UserController.updateUser)
    app.delete('/user/:id', isAuthenticated, UserController.deleteUser)

    //PS api
    // app.post('/ps', PSController.create);
    // app.get('/admin/users', isAuthenticated, UserController.getUserList)
    // app.get('/user/profile/contact/:id', isAuthenticated, UserController.getUser)
    // app.put('/user/profile/contact/:id', isAuthenticated, UserController.updateUser)
    // app.delete('/user/:id',isAuthenticated, UserController.deleteUser)

    app.post('/ps/', isAuthenticated, PSController.create)
    app.put('/ps/:id', isAuthenticated, PSController.update)
    app.get('/ps/match/:id', isAuthenticated, PSController.match)
    app.get('/ps/public', PSController.retrieveOpenPosts)
    app.get('/ps/drafts', isAuthenticated, PSController.retrieveDrafts)
    app.get('/ps/pending', isAuthenticated, PSController.retrievePendingPostsCount)
    app.get('/ps/post', isAuthenticated, PSController.retrievePosts)
    app.get('/ps/search', isAuthenticated, PSController.search)
    app.get('/ps/post/:id', isAuthenticated, PSController.retrieveByID);
    app.put('/ps/bid/:id', isAuthenticated, PSController.bid);
    app.put('/ps/accept/:id', isAuthenticated, PSController.accept);
    app.get('/ps/notify', isAuthenticated, PSController.getNotificationsCount);
    app.get('/ps/notifications', isAuthenticated, PSController.getNotifications);
    app.get('/ps/mybids/', isAuthenticated, PSController.getUserBids);
    app.get('/ps/pending/bids', isAuthenticated, PSController.getUserBidCount);


    //Company api
    app.put('/user/profile/company', isAuthenticated, CompanyController.update);
    app.get('/user/profile/company', isAuthenticated, CompanyController.retrieve)
    // app.get('/admin/users', isAuthenticated, UserController.getUserList)
    // app.get('/user/profile/contact/:id', isAuthenticated, UserController.getUser)
    // app.put('/user/profile/contact/:id', isAuthenticated, UserController.updateUser)
    // app.delete('/user/:id',isAuthenticated, UserController.deleteUser)


    //Country api
    app.get('/options/states', isAuthenticated, CountryController.retrieve)


    // app.post("/login", passport.authenticate("local", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: "/",
    //     failureFlash : true
    // }));

    // app.post("/reset-password", UserController.resetPasswd);

    // app.get('/home', isAuthenticated, function(req, res) {
    //     res.redirect('..' + HOME_PAGE);
    // });


    // app.get("/oauth/google", passport.authenticate("google", {
    //     scope: ["email", "profile"]
    // }));

    // app.get("/oauth/google/callback", passport.authenticate("google", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE
    // }));

    // app.get("/oauth/facebook", passport.authenticate("facebook", {
    //     scope: ["email", "public_profile"]
    // }));

    // app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/linkedin',
    //     passport.authenticate('linkedin', { state: 'SOME STATE'  }),
    //     function(req, res){
    //         // The request will be redirected to LinkedIn for authentication, so this
    //         // function will not be called.
    //     });

    // app.get('/oauth/linkedin/callback', passport.authenticate('linkedin', {
    //     successRedirect: HOME_PAGE,
    //     failureRedirect: SIGNIN_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/wechat', passport.authenticate('wechat'));

    // app.get('/oauth/wechat/callback', passport.authenticate('wechat', {
    //     failureRedirect: SIGNIN_PAGE,
    //     successReturnToOrRedirect: HOME_PAGE,
    //     failureFlash : true
    // }));

    // app.get('/oauth/twitter', passport.authenticate('twitter'));

    // // handle the callback after twitter has authenticated the user
    // app.get('/oauth/twitter/callback',
    //     passport.authenticate('twitter', {
    //         successRedirect : HOME_PAGE,
    //         failureRedirect : SIGNIN_PAGE
    //     }));

    app.get("/status/user", function (req, res) {
        var status = "";
        // console.log(req)
        if (req.user) {
            status = req.user.username;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    app.get("/user/logout", function (req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });


    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        // res.redirect(SIGNIN_PAGE);
        res.status(401).json({ message: "Unauthorized" });
    }
    app.use(function (req, res, next) {
        if (req.user == null) {
            // res.redirect(SIGNIN_PAGE);
            res.status(401).json({ message: "Unauthorized" });
        }
        next();
    });

};
