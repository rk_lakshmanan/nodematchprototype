// Read configurations
var config = require('./config');

// var MongoClient = require('mongodb').MongoClient;
// var url = "mongodb://localhost:27017/mydb";
// var User;
// var Company;
// var db;

// function initialize(inputDb) {
//     db = inputDb;
//     this.User = db.collection(config.USER_PROFILE)
//     this.Company = db.collection(config.COMPANY_PROFILE)
// }

module.exports = function connect(app) {
    var assert = require('assert');
    // Read configurations
    var config = require('./config');
    // Load MongoDB Nodejs Driver - MongoClient
    var MongoClient = require('mongodb').MongoClient;
    // MongoDB Connection URL
    // var url = config.MONGO_URI;
    var url = config.DATABASE_URI;
    // console.log("url: " + url);
    // Create the database connection
    MongoClient.connect(url, {
        poolSize: 10
        // other options can go here
    }, function (err, db) {
        assert.equal(null, err);
        console.log("MongoDB connection established")
        app.locals.db = db; // access using req.app.local.db at the routes
        require("./seed.js")(db);
    });
    // return MongoClient.connect(url, {
    //     poolSize: 10
    //     // other options can go here
    // })
}

// module.exports = {
//     User: User,
//     Company: Company,
//     initialize: initialize,
//     connect: connect,
// }