var mongo = require('mongodb');
var config = require('../config');

var PUBLISH_STATUS = {
    OPEN: "open",
    CLOSED: "closed",
    DRAFT: "draft",
}

var NOTIFIED_STATUS = {
    UNREAD: "unread",
    READ: "read",
}

//Collection variables
var PS;
var Company
function init(req, callee) {
    p(req, callee);
    const db = req.app.locals.db;
    if (!PS) {
        PS = db.collection(config.PS_DB);
    }
    if (!Company) {
        Company = db.collection(config.COMPANY_DB);
    }
}

function p(req, callee) {
    console.log(req.method + " " + req.url + ": " + callee);
}

function create() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var dataDetails = req.body.form;
        dataDetails.platform = req.body.platform.platform;
        // console.log("dataDetails")
        // console.log(req.body);
        dataDetails.publishStatus = PUBLISH_STATUS.OPEN;
        dataDetails.buyer_id = req.body.userid;
        dataDetails.bidders = [];
        // console.log(dataDetails);
        PS
            .insertOne(dataDetails)
            .then((result) => {
                // console.log(result.insertedId);
                res.status(200).json({
                    id: result.insertedId
                });
                // res.status(200).json({
                //     _d:"Successfully created"
                // });
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to create"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });

    }
}

function update() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var dataDetails = req.body.details;
        dataDetails.publishStatus = PUBLISH_STATUS.OPEN;
        console.log(dataDetails);
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        var query = { _id: new mongo.ObjectID(req.params.id) };
        var options = { upsert: true };
        var options;
        PS
            .updateOne(query, { $set: dataDetails }, options)
            // .updateOne(query, {$set:{dataDetails}})
            .then((result) => {
                // console.log(result.result);
                res.status(200).json(result.result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to update"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}

function remove() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var dataDetails = req.body.details;
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        var query = { _id: new mongo.ObjectID(req.params.id) };
        PS
            .remove(query, dataDetails)
            .then((result) => {
                console.log(result);
                // res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to update"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function retrieveDrafts() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var dataDetails = req.body.details;
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        // console.log(req.params.id);
        var query = {
            buyer_id: req.user._id,
            publishStatus: PUBLISH_STATUS.DRAFT
        }
    };
    var projection = { buyer_id: 0 };
    // var projection = null;
    PS
        // .find(query, projection).limit(50).toArray()
        .find(query, projection).limit(20).toArray()
        .then((result) => {
            // console.log(result);
            res.status(200).json(result);
        })
        .catch((err) => {
            console.log(err);
            errMsg = "Unable to "
            console.log(errMsg)
            res.status(500).json({
                message: errMsg,
            });
        });
}


function retrieveOpenPosts() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var dataDetails = req.body.details;
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        // console.log(req.params.id);
        var query = {
            $and: [
                { publishStatus: PUBLISH_STATUS.OPEN },
                { platform: "public" },
            ]
        };
        if (req.user) {
            query.$and.push({ "buyer_id": { $ne: req.user._id } })
        }

        var projection = { buyer_id: 0 };
        // var projection = null;
        PS
            // .find(query, projection).limit(50).toArray()
            .find(query, projection).limit(20).toArray()
            .then((result) => {
                // console.log(result);
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to "
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}

function assignKeywords(keywords, arr) {
    for (var i = 0; i < arr.length; i++) {
        keywords.push(arr[i]);
    }
    return keywords;
}
// function generateIndex(inputObj) {
function generateIndex() {
    var indexObj = {
        // type_of_partnership_interested: "text",
        '$**': "text",
    };
    // for (var i in inputObj) {
    //     if (isValidIndex(i)) {
    //         console.log(i + " " + inputObj[i]);
    //         indexObj[i] = "text";
    //     }
    // }
    // console.log("indexObj: ");
    // console.log(indexObj);
    return indexObj;
}
function checkIndexExists(collection) {
    // return PS.indexExists(generateIndex(inputObj));
    return collection.indexExists(generateIndex());
}
function isValidIndex(property) {
    var invalidIndexes = {
        platform: true,
        publishStatus: true,
        requirement: true,
        _id: true,
        buyer_id: true,
        seller_id: true,
    }
    if (invalidIndexes[property]) {
        return false;
    }
    return true;
}
function generateTextQuery(inputObj) {
    // var str = ""
    // for(var i = 0;i<keywords.length;i++){
    //     str+="\""+keywords+"\" ";
    // }
    // return keywords;
    // console.log(str);
    // return str;
    var str = "";
    for (var i in inputObj) {
        if (isValidIndex(i)) {
            // console.log(i + " " + inputObj[i]);
            str += " " + inputObj[i];
        }
    }
    return str;
}
function invertPostType(postType) {
    if (postType == "offer") {

        return "request";
    }
    else {
        return "offer";
    }
}
function findCompanyMatch(req, res, psObjToMatch, finalResult) {
    // console.log('company')
    var comTextQuery = { $text: { $search: generateTextQuery(psObjToMatch) } };
    var comQueryObj = {
        $and: [
            comTextQuery,
            // { "key_employee_id": { $ne: new mongo.ObjectID(psObjToMatch.buyer_id) } },
            { "key_employee_id": { $ne: psObjToMatch.buyer_id } },
        ]
    }
    var sortOptions = {
        score: { $meta: "textScore" }
    }

    //Actually match
    Company
        .find(comQueryObj, sortOptions)
        .sort(sortOptions)
        // .limit(1)
        .toArray()
        .then((companyMatchResult) => {
            // console.log(companyMatchResult);
            finalResult.company = companyMatchResult;
            // findCompanyMatch(req, res, psObjToMatch, finalResult)
            // console.log("finalResult")
            // console.log(finalResult)
            res.status(200).json(finalResult);
        })
        .catch((err) => {
            console.log(err);
        });


}
function findPSMatch(req, res, psObjToMatch, finalResult) {
    var psTextQuery = { $text: { $search: generateTextQuery(psObjToMatch) } };
    var psQueryObj = {
        $and: [
            psTextQuery,
            { "post_type": { $ne: psObjToMatch.post_type } },
            { "_id": { $ne: new mongo.ObjectID(psObjToMatch._id) } },
            { "buyer_id": { $ne: req.user._id } },
        ]
    }
    var sortOptions = {
        score: { $meta: "textScore" }
    }
    // console.log('psQueryObj: ');
    // console.log(psQueryObj);

    //Actually match
    PS
        .find(psQueryObj, sortOptions)
        .sort(sortOptions)
        // .limit(1)
        .toArray()
        .then((psMatchResult) => {
            // console.log(psMatchResult);
            // res.status(200).json(result);
            finalResult.post = psMatchResult;
            checkIndexExists(Company)
                .then((doesComIndexExist) => {
                    // console.log('doesComIndexExist: ' + doesComIndexExist);
                    if (!doesComIndexExist) {

                        Company.createIndex(generateIndex())
                            .then((companyIndexResult) => {
                                // console.log("companyIndexResult " + companyIndexResult);
                                findCompanyMatch(req, res, psObjToMatch, finalResult)
                            })
                            .catch((err) => {
                                console.log(err);
                            });

                    } else {
                        findCompanyMatch(req, res, psObjToMatch, finalResult)
                    }

                })
        })
        .catch((err) => {
            console.log(err);
        });


    // res.status(200).json(result);


}

function match() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var ps_id = req.params.id;
        console.log("ps_id is " + ps_id);
        var query = { _id: new mongo.ObjectID(ps_id) };
        var projection = null;
        var finalResult = {};
        PS
            // .find(query).limit(50).toArray()
            .findOne(query)
            .then((psObjToMatch) => {
                // console.log(psObjToMatch);
                // checkIndexExists(psObjToMatch)
                checkIndexExists(PS)
                    .then((doesIndexExists) => {
                        // console.log("doesIndexExists: " + doesIndexExists);
                        if (!doesIndexExists) {
                            // PS.index(generateIndex(psObjToMatch))
                            PS.createIndex(generateIndex())
                                .then((indexResult) => {
                                    // console.log('indexResult: ' + indexResult);
                                    findPSMatch(req, res, psObjToMatch, finalResult);
                                })
                                .catch((err) => {
                                    console.log(err);
                                });
                        } else {
                            findPSMatch(req, res, psObjToMatch, finalResult);
                        }
                    })
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to update"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}

function searchCompany(req, res, textQuery, dataDetails, finalResult) {
    // var comTextQuery = { $text: { $search: generateTextQuery() } };
    var comTextQuery = { $text: { $search: textQuery } };
    var comQueryObj = {
        $and: [
            comTextQuery,
            { "key_employee_id": { $ne: req.user._id } },
        ]
    }
    var sortOptions = {
        score: { $meta: "textScore" }
    }
    // console.log('psQueryObj: ');
    // console.log(psQueryObj);

    //Actually match
    Company
        .find(comQueryObj, sortOptions)
        .sort(sortOptions)
        // .limit(1)
        .toArray()
        .then((companyMatchResult) => {
            console.log(companyMatchResult);
            finalResult.company = companyMatchResult;
            // findCompanyMatch(req, res, psObjToMatch, finalResult)
            console.log("finalResult")
            console.log(finalResult)
            res.status(200).json(finalResult);
        })
        .catch((err) => {
            console.log(err);
        });


}

function searchPS(req, res, textQuery, dataDetails, finalResult) {
    // var psTextQuery = { $text: { $search: generateTextQuery(psObjToMatch) } };
    var andObj = [];
    //rfx option
    if (dataDetails.rfx_option != "all") {
        andObj.push({ "requirement": dataDetails.rfx_option });
    }
    //post_option
    if (dataDetails.post_option != "all") {
        andObj.push({ post_type: dataDetails.post_option });
    }
    //text search
    var psTextQuery = { $text: { $search: textQuery } };
    andObj.push(psTextQuery);
    // not your own id
    andObj.push({ "buyer_id": { $ne: req.user._id } });
    //post should not be private
    andObj.push({ "platform": "public" })
    //publishStatus should be open
    andObj.push({ "publishStatus": "open" })

    // console.log("andObj")
    // console.log(andObj)

    var psQueryObj = {
        $and: andObj,
    }
    // console.log("psquerObj")
    // console.log(psQueryObj);
    var sortOptions = {
        score: { $meta: "textScore" }
    }

    PS
        .find(psQueryObj, sortOptions)
        .sort(sortOptions)
        // .limit(1)
        .toArray()
        .then((psMatchResult) => {
            // console.log(psMatchResult);
            res.status(200).json(psMatchResult);
            // finalResult.post = psMatchResult;
            // checkIndexExists(Company)
            //     .then((doesComIndexExist) => {
            //         console.log('doesComIndexExist: ' + doesComIndexExist);
            //         if (!doesComIndexExist) {
            //             Company.createIndex(generateIndex())
            //                 .then((companyIndexResult) => {
            //                     console.log("companyIndexResult " + companyIndexResult);
            //                     // searchCompany(req, res, textQuery, dataDetails, finalResult)
            //                 })
            //                 .catch((err) => {
            //                     console.log(err);
            //                 });

            //     } else {
            //         // searchCompany(req, res, textQuery, dataDetails, finalResult)
            //     }

            // })
        })
        .catch((err) => {
            console.log(err);
        });
}
//category_option:"partnership" or "product/service"
//if you partnership, you use post option (req or offer) and rfx option
//if you product, use post option and pdt option (NOT USED)
function search() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // console.log(req.query)
        // console.log("ps_id is " + ps_id);
        // var query = { _id: new mongo.ObjectID(ps_id) };
        // var projection = null;
        var dataDetails = JSON.parse(req.query.filter);
        // console.log('dataDetails')
        // console.log(dataDetails);
        var textQuery = req.query.string;
        var finalResult = {};
        checkIndexExists(PS)
            .then((doesSearchIndexExists) => {
                // console.log("doesSearchIndexExists: " + doesSearchIndexExists);
                if (!doesSearchIndexExists) {
                    // PS.index(generateIndex(psObjToMatch))
                    PS.createIndex(generateIndex())
                        .then((indexResult) => {
                            // console.log('indexResult: ' + indexResult);
                            searchPS(req, res, textQuery, dataDetails, finalResult);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    searchPS(req, res, textQuery, dataDetails, finalResult);
                }
            })
    }
}




function retrievePendingPostsCount() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var query = {
            buyer_id: req.user._id,
            publishStatus: PUBLISH_STATUS.OPEN,
        }
        var projection = { buyer_id: 0 };
        query.post_type = "offer"
        var finalResult = {};
        PS
            .count(query)
            .then((resultOff) => {
                // console.log("number of pending posts: "+result);
                finalResult.offerCount = resultOff;
                query.post_type = "request";
                PS.count(query).then((resultReq) => {
                    finalResult.requestCount = resultReq;
                    res.status(200).json(finalResult);
                }).catch((err) => {
                    console.log(err)
                })
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to count pending posts"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function retrievePosts() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var post_type = req.query.post_type;
        var query = {
            buyer_id: req.user._id,
            // publishStatus: PUBLISH_STATUS.OPEN,
            post_type: post_type,
        }
        var projection = { buyer_id: 0 };
        var finalResult = {};
        PS
            .find(query)
            .toArray()
            .then((result) => {
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to retrieve posts"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function generatePublicPS(psObj) {
    // console.log(psObj)
    psObj.bidders_count = psObj.bidders ? psObj.bidders.length : 0;
    delete psObj.buyer_id;
    delete psObj.bidders;
    delete psObj.final_bidder;
    // console.log("final public psObj")
    // console.log(psObj)
    return psObj;
}

function findCompanyName(req, res, psObj) {
    // var company_ids = psObj.bidders.map((bidder_id) => {
    //     return new mongo.ObjectID(bidder_id);
    // })
    // console.log("company_ids");
    // console.log(company_ids)
    var company_ids = [];
    for (var i = 0; i < psObj.bidders.length; i++) {
        // company_ids.push(new mongo.ObjectID(psObj.bidders[i]));
    }
    // console.log("psObj bidders");
    // console.log(psObj.bidders)
    // console.log(company_ids[0])
    var projection = { company_name: 1 };
    // var query = { key_employee_id: { $in: company_ids } };
    var query = { key_employee_id: { $in: psObj.bidders } };
    // var query = { _id: { $in: company_ids } };
    Company
        .find(query, projection)
        .toArray()
        .then((result) => {
            console.log(result);
            psObj.company_names = result;
            res.status(200).json(psObj);
        })
        .catch((err) => {
            console.log(err);
        });

}

//if the user is NOT viewing his/her own post, 
//we will NOT return buyer,bidder and final_bidder info
function retrieveByID() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var ps_id = req.params.id;
        // console.log('ps_id: '+ps_id);

        var query = { _id: new mongo.ObjectID(ps_id) };
        // var projection = { buyer_id: 0 }
        // var finalResult = {};
        PS
            // .find(query).limit(50).toArray()
            // .findOne(query, projection)
            .findOne(query)
            .then((result) => {
                // console.log(result)
                if (result && result.buyer_id == req.user._id && result.bidders) {
                    // findCompanyName(res, req, result, finalResult);
                    findCompanyName(req, res, result);
                } else if (result && result.bidders && result.bidders.includes(req.user._id)) {
                    var tempPsObj = generatePublicPS(result);
                    tempPsObj.bidded = true;
                    res.status(200).json(tempPsObj);
                } else if (result && result.buyer_id != req.user._id) {
                    res.status(200).json(generatePublicPS(result));
                } else {
                    res.status(200).json(result);
                }
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to retrieveByID"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function isBiddable(result) {
    if (result.bidders) {
        if (result.bidders.length < 10) {
            return true;
        }
        else {
            return false;
        }
    }
    return true;
}
function bid() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var ps_id = req.params.id;
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        var userid = req.user._id;
        var query = { _id: new mongo.ObjectID(ps_id) };
        var objToSet = { $addToSet: { bidders: req.user._id } }
        // var options = { upsert: true };
        var options;
        PS
            .findOne(query)
            .then((result) => {
                // console.log(result);
                if (isBiddable(result)) {
                    // return PS.updateOne(query, objToSet, options);
                    PS
                        .updateOne(query, objToSet)
                        .then((result) => {
                            // console.log(result.result);
                            res.status(200).json(result.result);
                        })
                        .catch((err) => {
                            console.log(err);
                            var errMsg = "Unable to bid"
                            console.log(errMsg)
                            res.status(500).json({
                                message: errMsg,
                            });
                        });
                } else {
                    res.status(500).json({
                        message: "bid limit reached"
                    })
                }
            }).catch((err) => {
                console.log(err)
                res.status(500).json({ message: "unable to bid" })
            })
        // .updateOne(query, objToSet, options)
        // .updateOne(query, {$set:{dataDetails}})

    }
}

function accept() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var ps_id = req.params.id;
        var final_bidder = req.body.bidder_id;
        var query = {
            key_employee_id: final_bidder,
        }


        Company
            .findOne(query)
            .then((findResult) => {
                var queryUpdate = { _id: new mongo.ObjectID(ps_id) };
                var objToSet = {
                    $set: {
                        final_bidder: final_bidder,
                        publishStatus: PUBLISH_STATUS.CLOSED,
                        notified_status: NOTIFIED_STATUS.UNREAD,
                        final_bidder_company_name: findResult.company_name,
                    }
                }
                return PS.updateOne(queryUpdate, objToSet)

            })
            .then((result) => {
                res.status(200).json(result.result);
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to accept"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}

function getNotificationsCount() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var final_bidder = req.user._id;
        var query = {
            final_bidder: final_bidder,
            publishStatus: PUBLISH_STATUS.CLOSED,
            notified_status: NOTIFIED_STATUS.UNREAD,
        }
        PS
            .count(query)
            .then((result) => {
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to getNotificationsCount"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}

function getNotifications() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var  = req.params.id;
        var final_bidder = req.user._id;
        // console.log('ps_id: '+ps_id);

        var query = {
            final_bidder: final_bidder,
            publishStatus: PUBLISH_STATUS.CLOSED,
            // notified_status: NOTIFIED_STATUS.UNREAD,
        }
        var notificationList;
        var toSet = {
            $set: {
                notified_status: NOTIFIED_STATUS.READ,
            }
        }
        PS
            .find(query)
            .toArray()
            // .updateOne(query, {$set:{dataDetails}})
            .then((result) => {
                // console.log(result.result);
                // res.status(200).json(result);
                // res.status(200).json({ success: true });

                notificationList = result;
                var queryUpdate = {
                    final_bidder: final_bidder,
                    publishStatus: PUBLISH_STATUS.CLOSED,
                    notified_status: NOTIFIED_STATUS.UNREAD,
                }
                return PS.updateMany(queryUpdate, toSet);
            })
            .then((updateResult) => {
                // console.log(updateResult);
                res.status(200).json(notificationList);
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to find notifications"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function getUserBids() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var  = req.params.id;
        // var  = req.user._id;
        // console.log('ps_id: '+ps_id);

        // console.log(req.user._id);
        var query = {
            // bidders: { $elemMatch:{ req.user._id }}
            bidders: { $in: [req.user._id] }
        }
        PS
            .find(query)
            .toArray()
            // .updateOne(query, {$set:{dataDetails}})
            .then((result) => {
                // console.log(result);
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to find user bids"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}
function getUserBidCount() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var  = req.params.id;
        // var  = req.user._id;
        // console.log('ps_id: '+ps_id);

        // console.log(req.user._id);
        var query = {
            // bidders: { $elemMatch:{ req.user._id }}
            publishStatus: PUBLISH_STATUS.OPEN,
            bidders: { $in: [req.user._id] }
        }
        PS
            .count(query)
            // .toArray()
            // .updateOne(query, {$set:{dataDetails}})
            .then((result) => {
                // console.log(result);
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                var errMsg = "Unable to find user bids"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}



// Export route handlers
module.exports = {
    create: create(),
    update: update(),
    delete: remove(),
    match: match(),
    retrieveOpenPosts: retrieveOpenPosts(),
    retrieveDrafts: retrieveDrafts(),
    retrievePendingPostsCount: retrievePendingPostsCount(),
    retrievePosts: retrievePosts(),
    retrieveByID: retrieveByID(),
    search: search(),
    bid: bid(),
    accept: accept(),
    getNotificationsCount: getNotificationsCount(),
    getNotifications: getNotifications(),
    getUserBids: getUserBids(),
    getUserBidCount: getUserBidCount(),
};