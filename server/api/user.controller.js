var mongo = require('mongodb');
var config = require('../config');

var bcrypt = require('bcryptjs');

//Collection variables
var User;
var Company;
// function removePassword(userDetails) {
//   delete userDetails["password"];
//   return userDetails;
// }


function init(req,callee) {
  p(req,callee);
  const db = req.app.locals.db;
  if (!User) {
    // User = req.app.locals.db.collection(config.USER_DB)
    User = db.collection(config.USER_DB)
  }
  if (!Company) {
    Company = db.collection(config.COMPANY_DB);
  }
}

function p(req,callee) {
  console.log(req.method + " " + req.url+": "+callee);
}

function mail(dataToInsert) {
  var api_key = config.MAILGUN_API_KEY;
  // var domain = 'www.mydomain.com';
  var domain = "sandbox840cb49f5be64a72aa230be136b987f5.mailgun.org";
  var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

  var data = {
    from: 'Excited User <me@samples.mailgun.org>',
    // to: 'laksh@nodex.sg',
    to: 'laksh@nodex.sg',
    subject: 'Hello',
    text: 'Testing some Mailgun awesomness!'
  };
  console.log("sending mailgun")
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if (error) {
      console.log("mailgun error")
      console.log(error);
    }
  });
}

function createCompanyProfile(dataToInsert, key_employee_id) {
  Company.insertOne({
    company_name: dataToInsert.company_name,
    key_employee_id: key_employee_id
  }).then((comProfileResult)=>{
    // console.log(comProfileResult) 
  }).catch((error)=>{
    console.log(error); 
  })
}

function register(){
  return (req, res) => {
    init(req, arguments.callee.name);
    var userDetails = req.body.details;
    // console.log(userDetails);
    if (userDetails.password != userDetails.confirm_password) {
      res.status(500).json({
        err: "Passwords don't match"
      })
    }
    var hashPassword = bcrypt.hashSync(userDetails.password, bcrypt.genSaltSync(8), null);
    var dataToInsert = {
      salutation: userDetails.salutation,
      first_name: userDetails.first_name,
      last_name: userDetails.last_name,
      username: userDetails.username, //email address
      password: hashPassword,
      company_name: userDetails.company_name,
      privilege: "member",
    }
    // console.log(dataToInsert)
    // console.log(User)
    User
      .findOne({ username: userDetails.username })
      .then((result) => {
        if (result) {
          console.log(result)
          console.log("username already exist")
          res.status(500).json({
            message: "Username already exists",
          })
        }
        else {
          User
            .insertOne(dataToInsert)
            .then((result) => {
              // console.log("successfully added" + result);
              // console.log(result)
              // console.log(result)
              // mail(dataToInsert);
              createCompanyProfile(dataToInsert, result.insertedId.toString());
              res.status(200).json({
                success: true,
              })
            })
            .catch((err) => {
              console.log(err);
              console.log("unable to create")
              res.status(500).json({
                message: "Unable to create account"
              })
            })
        }
      })
      .catch((err) => {
        console.log("unable to create")
        res.status(500).json({
          message: "Unable to create account"
        })

      })

  }
}
function updateUser() {
  return (req, res) => {
    init(req, arguments.callee.name);
    var id = new mongo.ObjectID(req.params.id);
    // console.log(id);
    var details = req.body.details;
    // console.log(details);
    // console.log(userDetails);
    var query = { _id: id };
    var toChange = { $set: details }
    // var toChange = { $set:{ first_name:"hello"}}
    // var options = { upsert: false, returnOriginal: false };
    User
      // .findOne(query, toChange)
      // .findOne(query)
      .updateOne(query, toChange)
      // .findOneAndUpdate(query, toChange,options)
      .then((results) => {
        // console.log('then')
        // console.log(results);
        res.status(200).json(results.result);
        // User.findOne(query).then((results)=>{
        //   console.log("---------------------")
        //   console.log(results)
        // })
      })
      .catch((err) => {
        console.log(err);
        console.log("unable to update")
        res.status(500).json({
          message: "Unable to update profile"
        })
      })


  }
}
function getUser() {
  return (req, res) => {
    init(req, arguments.callee.name);
    var id = new mongo.ObjectID(req.params.id);
    // console.log(id);
    // var details = req.body.details;
    // console.log(details);
    // console.log(userDetails);
    var query = { _id: id };
    // var toChange = { $set: details }
    // var toChange = { $set:{ first_name:"hello"}}
    // var options = { upsert: false, returnOriginal: false };
    var projection = { password: 0, reset_password_token: 0 };
    User
      // .findOne(query, toChange)
      .findOne(query, projection)
      // .updateOne(query, toChange)
      // .findOneAndUpdate(query, toChange,options)
      .then((results) => {
        // console.log(results);
        // res.status(200).json(removePassword(results));
        res.status(200).json(results);
        // User.findOne(query).then((results)=>{
        //   console.log("---------------------")
        //   console.log(results)
        // })
      })
      .catch((err) => {
        console.log(err);
        console.log("unable to update")
        res.status(500).json({
          message: "Unable to update profile"
        })
      })


  }
}

function getUserList() {
  return (req, res) => {
    init(req, arguments.callee.name);
    // var query = {}
    var query = { privilege: { $ne: "admin" } }
    var projection = { password: 0, reset_password_token: 0 };
    User
      .find(query, projection).limit(50).toArray()
      .then((results) => {
        console.log(results);
        res.status(200).json(results);
      })
      .catch((err) => {
        console.log(err);
        var msg = "Unable to get userList";
        console.log(msg)
        res.status(500).json({
          message: msg,
        })
      })
  }
}

function deleteUser() {
  return (req, res) => {
    init(req, arguments.callee.name);
    var id = new mongo.ObjectID(req.params.id);
    var query = { _id: id }
    User
      .remove(query)
      .then((results) => {
        // console.log(results);
        res.status(200).json(results.result);
      })
      .catch((err) => {
        console.log(err);
        var msg = "unable to remove user";
        console.log(msg)
        res.status(500).json({
          message: msg,
        })
      })
  }
}






// Retrieve all the products - GET /api/products
var retrieveAll = function () {
  return function (req, res) {
    console.log("Route: GET /api/products");
    // Get the database object
    const db = req.app.locals.db;
    // *** Exercise ***
    // Requirements:
    // 1. Retrieve records from the restaurant list
    // 2. Projection - {name: 1, cuisine: 1, restaurant_id: 1, "address.street": 1, borough: 1}
    // 3. Limit to only 50 records
    // 4. Convert the records to array and send it back to client (Service: retrieveAll)
    // Validation:
    // 1. On the Search page, clicking on the "Retrieve Products" button will retrieve restaurant list

    // Enter your codes here

    // init(db);
    // collection.find({}).limit(50).toArray((err,item)=>{
    //    if(err){
    //        console.log(err);
    //    }else{
    //        console.log(item);

    //    } 
    // })

    init(db);
    collection.find({},
      { name: 1, cuisine: 1, restaurant_id: 1, "address.street": 1, borough: 1 })
      .limit(50)
      .toArray()
      .then((results) => {
        // console.log(results);
        res.status(200).json(results);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });


  }
};

// Retrieve one product using objectID - GET /api/products/:id
var retrieveOne = function () {
  return function (req, res) {
    console.log("Route: GET /api/products/:id");
    console.log("Object ID to retrieve: " + req.params.id);
    // Convert to object ID type
    var param = new mongo.ObjectID(req.params.id);
    // Get the database object
    const db = req.app.locals.db;

    // *** Exercise ***
    // Requirements:
    // 1. Retrieve a record from the restaurant list based on the object ID (URL parameter) that is passed
    // 2. Return all the fields back to client (Service: retrieveProduct(id))
    // Validation:
    // 1. On the Search page, clicking on "Name" field of the record
    // 2. You should see the detail page showing all the record information

    // Enter your codes here
    init(db);
    collection.findOne({ _id: param })
      .then((results) => {
        // console.log(results);
        res.status(200).json(results);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });
  }
};

// Insert one product (Pass the product document using JSON body) - POST /api/products
var insertOne = function () {
  return function (req, res) {
    console.log("Route: POST /api/products");
    // Get the database object
    const db = req.app.locals.db;
    // Insert the new product (product JSON info in req.body)
    // console.log(req.body.newProduct)
    var doc = JSON.parse(req.body.newProduct);
    // const doc = req.app.locals.
    // *** Exercise ***
    // Requirements:
    // 1. Insert a record to the restaurant list based on the info send in req.body.newProduct
    // 2. Return status back to client (Service: addProduct(product))
    // Validation:
    // 1. On the Search page, click on the "Retrieve Products" button to retrieve restaurant list
    // 2. If you can't see the record, use the Postman to check as we have limit to 50 records

    // Enter your codes here
    init(db);
    collection.insertOne({
      name: doc.name,
      address: doc.address,
      borough: doc.borough,
      cuisine: doc.cuisine,
      grades: doc.grades

    })
      .then((results) => {
        console.log(results.results);
        res.status(200).json(results);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });

  }
};

// Update one product using objectID (set parameters in JSON body) - PUT /api/products
var updateOne = function () {
  return function (req, res) {
    console.log("Route: PUT /api/products/:id");
    console.log("Object ID to update: " + req.params.id);
    // Convert to object ID type
    var param = new mongo.ObjectID(req.params.id);
    var doc = JSON.parse(req.body.updateProduct); // convert json to object
    // Get the database object
    const db = req.app.locals.db;

    // *** Exercise ***
    // Requirements:
    // 1. Update a record on the restaurant list based on the info send in req.body.updateProduct
    //    and the id (URL parameter)
    // 2. Return status back to client (Service: updateProduct(id, product))
    // Validation:
    // 1. On the Search page, click on the "Retrieve Products" button to retrieve restaurant list
    // 2. Click on the Edit icon (pencil) of one record
    // 3. Update the record
    // 4. Return to the search page to check whether the record is updated

    // Enter your codes here
    console.log(doc);
    // console.log(param);
    init(db);
    collection.updateOne(
      { _id: param },
      {
        // $set: {
        //     name: doc.name,
        //     address: doc.address,
        //     borough: doc.borough,
        //     cuisine: doc.cuisine,
        //     grades: doc.grades,
        //     restaurant_id:doc.restaurant_id,
        // }
        $set: doc,
      })
      .then((results) => {
        // console.log(results);
        res.status(200).json(results);
      })
      .catch((err) => {
        // console.log(err);
        res.status(500).json(err);
      });
  }
};

// Insert new grade for product using objectID (set parameters in JSON body) - PUT /api/products/grades
var updateGrade = function () {
  return function (req, res) {
    console.log("Route: PUT /api/products/grades/:id");
    console.log("Object ID to update: " + req.params.id);
    // Convert to object ID type
    var param = new mongo.ObjectID(req.params.id);
    var doc = JSON.parse(req.body.newGrades); // convert json to object
    // Get the database object
    const db = req.app.locals.db;

    // *** Exercise ***
    // Requirements:
    // 1. Insert a new record to the "grades" array
    // 2. Return only the new array item back to client (Service: addGrade(id, grades))
    //    This allows the client to insert the new array item to the view
    // Validation:
    // 1. On the Search page, click on the "Retrieve Products" button to retrieve restaurant list
    // 2. Click on the "Name" field of one record to go to details page
    // 3. Click on "Add Grade" button. Enter the grade and score for the new grade record. 
    // 4. Click on "Submit Grade" button. If successful, you should see the new grade inserted
    // Hint: Use "findOneAndUpdate" method
    // console.log(req.body.newGrades)
    // Enter your codes here
    init(db);
    collection.findOneAndUpdate(
      { _id: param },
      {
        $push: {
          "grades": {
            grade: doc.grade,
            score: doc.score,
            date: doc.date,
          }
        }
      },
      // })
      { upsert: false, returnOriginal: false })
      .then((results) => {
        console.log(results);
        var newGrade = results.value.grades[results.value.grades.length - 1];
        res.status(200).json(newGrade);
        // res.status(200).json(results);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });
  }
};
// Delete one product using objectID - DELETE /api/products/:id
var deleteOne = function () {
  return function (req, res) {
    console.log("Route: DELETE /api/products/:id");
    console.log("Object ID to delete: " + req.params.id);
    // Convert to object ID type
    var param = new mongo.ObjectID(req.params.id);
    // Get the database object
    const db = req.app.locals.db;

    // *** Exercise ***
    // Requirements:
    // 1. Delete a record on the restaurant list based on the object ID (URL parameter)
    // 2. Return status back to client (Service: deleteProduct(id))
    // Validation:
    // 1. On the Search page, click on the "Retrieve Products" button to retrieve restaurant list
    // 2. Click on the Delete icon (bin) of one record
    // 3. Record will be deleted

    // Enter your codes here
    console.log(param);
    init(db);
    collection.deleteOne({ _id: param })
      .then((results) => {
        // console.log(results);
        res.status(200).json(results);
      })
      .catch((err) => {
        // console.log(err);
        res.status(500).json(err);
      });
  }
};

// Retrieve list of product types - GET /api/product/types
var retrieveTypes = function () {
  return function (req, res) {
    console.log("Route: GET /api/products/types");
    // Get the database object
    const db = req.app.locals.db;
    try {
      // db.collection(config.COLL_MAIN, function (err, collection) {
      // Hanlde collection err
      init(db);
      console.log("enter collection")
      // if (err) {
      //     res.status(500).send(err);
      // };
      // Field to find distinct value
      var key = "cuisine";
      // Query Operator
      var query = {};
      // Options
      var options = {};
      // Retrieve distinct types
      collection.distinct(key, query, options)
        .then(function (results) {
          // console.log(results.sort());
          res.json(results.sort());
        })
        .catch(function (err) {
          console.log("Error retrieving distinct types: " + err);
          res.status(500).send(err);
        })
      // })
    } catch (err) {
      console.log("Runtime errors for retrieve distinct types: " + err);
      res.status(500).send(err);
    }
  }
};

// Export route handlers
module.exports = {
  // retrieveAll: retrieveAll(),
  // retrieveOne: retrieveOne(),
  // insertOne: insertOne(),
  // updateOne: updateOne(),
  // deleteOne: deleteOne(),
  // retrieveTypes: retrieveTypes(),
  // updateGrade: updateGrade()
  // create: create(),
  register: register(),
  getUser: getUser(),
  getUserList: getUserList(),
  deleteUser: deleteUser(),
  updateUser: updateUser(),
};

