var mongo = require('mongodb');
var config = require('../config');

//Collection variables
var Company;
function init(req, callee) {
    p(req, callee);
    const db = req.app.locals.db;
    if (!Company) {
        Company = db.collection(config.COMPANY_DB, callee);
    }
}

function p(req, callee) {
    console.log(req.method + " " + req.url + ": " + callee)
}

function create() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var dataDetails = req.body.details;
        dataDetails._id = new mongo.ObjectID(req.params.id);
        Company
            .insertOne(dataDetails)
            .then((result) => {
                console.log(result);
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to create"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });

    }
}

function update() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var key_employee_id = req.body.userid;
        var query = { key_employee_id: key_employee_id };
        var dataDetails = req.body.details;
        dataDetails.key_employee_id = key_employee_id;
        var options = { upsert: true };
        Company
            .update(query, dataDetails, options)
            .then((result) => {
                res.status(200).json(result.result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to update"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}


function remove() {
    return (req, res) => {
        init(req, arguments.callee.name);
        // var dataDetails = req.body.details;
        // dataDetails._id = new mongo.ObjectID(req.params.id);
        var query = { _id: new mongo.ObjectID(req.params.id) };
        Company
            .remove(query, dataDetails)
            .then((result) => {
                console.log(result);
                // res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to update"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}


function retrieve() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var query = { key_employee_id: req.query.id };
        var projection = null;
        Company
            // .findOne(query, projection).limit(50).toArray()
            .findOne(query)
            .then((result) => {
                // console.log(result);
                res.status(200).json(result);
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to retrieve"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}



// Export route handlers
module.exports = {
    create: create(),
    update: update(),
    delete: remove(),
    retrieve: retrieve(),
};