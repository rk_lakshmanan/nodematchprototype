var mongo = require('mongodb');
var config = require('../config');

//Collection variables
var Country;
function init(req, callee) {
    p(req, callee);
    const db = req.app.locals.db;
    if (!Country) {
        Country = db.collection(config.COUNTRY_DB, callee);
    }
}

function p(req, callee) {
    console.log(req.method + " " + req.url + ": " + callee)
}

function retrieve() {
    return (req, res) => {
        init(req, arguments.callee.name);
        var query = { country: req.query.country };
        Country
            .findOne(query)
            .then((result) => {
                if (result) {
                    res.status(200).json(result.states);
                }
                else {
                    res.status(401).json({
                        message: "States not found"
                    })
                }
            })
            .catch((err) => {
                console.log(err);
                errMsg = "Unable to retrieve"
                console.log(errMsg)
                res.status(500).json({
                    message: errMsg,
                });
            });
    }
}



// Export route handlers
module.exports = {
    retrieve: retrieve(),
};