(function () {
    angular
        .module('NodeMatch')
        .controller('NavCtrl', NavCtrl);

    NavCtrl.$inject = ["UserService", "user", "$state"];

    function NavCtrl(UserService, user, $state) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        vm.notices = 0;
        vm.noticeList = [];

        notifications();

        // Functions
        vm.logout = logout;
        vm.notifications = notifications;
        vm.notified = notified;

        // Function declaration and definition
        function logout() {
            UserService
                .logout()
                .then(function(result){
                    $state.go("Login");
                })
        };

        function notifications(){
            if(vm.userid)
            UserService
                .notifications()
                .then(function(result){
                    // console.log(result);
                    vm.notices = result.data;
                })
                .catch(function(error){
                    // console.log(error);
                })
        }

        function notified(){
            if(vm.userid)
            UserService
                .notified()
                .then(function(result){
                    // console.log(result);
                    vm.notices = 0;
                    vm.noticeList = result.data;
                })
                .catch(function(error){
                    // console.log(error);
                })
        }

    }
})();
