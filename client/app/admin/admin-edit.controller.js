(function () {
    angular
        .module('NodeMatch')
        .controller('AdminEditCtrl', AdminEditCtrl);

    AdminEditCtrl.$inject = ["$state", "$stateParams", "UserService", "user"];

    function AdminEditCtrl($state, $stateParams, UserService, user) {

        var vm = this;

        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        vm.usermemberid = $stateParams.id;
        vm.user = {
            salutation: null,
            first_name: "",
            last_name: "",
            company_name: "",
            privilege: null
        };

        vm.salutations = [
            { value: "Mr", name: "Mr." },
            { value: "Mrs.", name: "Mrs." },
            { value: "Ms.", name: "Ms." }
        ];

        vm.privileges = [
            { value: "member", name: "Member" },
            { value: "admin", name: "Admin" }
        ]

        vm.getUserOne = getUserOne;
        vm.editUser = editUser;
        vm.deleteUser = deleteUser;

        // Initialise
        if ($stateParams.id) {
            getUserOne();
        }

        function getUserOne() {
            UserService
                .getProfileContact(vm.usermemberid)
                .then(function (result) {
                    result = result.data;
                    var details = {
                        salutation: result.salutation,
                        first_name: result.first_name,
                        last_name: result.last_name,
                        company_name: result.company_name,
                        privilege: result.privilege,
                    };
                    vm.user = details;
                })
                .catch(function (err) {

                })
        }

        function editUser() {
            UserService
                .editProfileContact(vm.usermemberid, vm.user)
                .then(function (result) {
                    console.log(result);
                    $state.go("Admin");
                })
                .catch(function (err) {
                    console.log(err)
                })
        }

        function deleteUser() {
            console.log("deleting");
            UserService
                .deleteUser(vm.usermemberid)
                .then(function (result) {
                    console.log(result);
                    $state.go("Admin")
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

    }
})();