(function () {
    angular
        .module('NodeMatch')
        .controller('AdminCtrl', AdminCtrl);

    AdminCtrl.$inject = ["$state", "UserService", "user"];

    function AdminCtrl($state, UserService, user) {

        var vm = this;

        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        vm.search = "";
        vm.userList = [];

        vm.getUserList = getUserList;
        vm.logout = logout;
        
        getUserList();
        function getUserList() {
            UserService
                .getUserList(vm.search)
                .then(function (result) {
                    vm.userList = result.data;
                    console.log(result)
                })
                .catch(function (err) {

                })
        };

        function logout() {
            UserService
                .logout()
        };

    }
})();