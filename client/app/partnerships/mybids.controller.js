(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsMyBidsCtrl', PartnershipsMyBidsCtrl);

    PartnershipsMyBidsCtrl.$inject = ["$stateParams", "$state", "PartnershipSvc", "user"];

    function PartnershipsMyBidsCtrl($stateParams, $state, PartnershipSvc, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        vm.bidList = [];

        mybids();
        // Functions
        vm.mybids = mybids;

        // Function declaration and definition
        function mybids(){
            PartnershipSvc
                .mybids()
                .then(function(result){
                    console.log(result);
                    vm.bidList = result.data;
                })
                .catch(function(error){
                    console.log(error);
                })
        }
    }
})();