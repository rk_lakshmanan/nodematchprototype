(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsOnePostCtrl', PartnershipsOnePostCtrl);

    PartnershipsOnePostCtrl.$inject = ["$stateParams", "$state", "PartnershipSvc", "user"];

    function PartnershipsOnePostCtrl($stateParams, $state, PartnershipSvc, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        
        vm.view_type = $stateParams.view;
        vm.post_id = $stateParams.id;
        vm.post_data = {};
        vm.selectedBidder = "";
        vm.bidtext = "";

        viewOnePost();

        // Functions
        vm.viewOnePost = viewOnePost;
        vm.acceptPost = acceptPost;
        vm.chooseBidder = chooseBidder;

        // Function declaration and definition
        function viewOnePost() {
            PartnershipSvc
                .viewOnePost(vm.post_id)
                .then(function(result){
                    console.log(result);
                    vm.post_data = result.data;
                    vm.bidtext = result.data.bidded ? "Bidded" : "Accept";
                })
                .catch(function(error){

                })
        }

        function acceptPost(){
            PartnershipSvc
                .acceptPost(vm.post_id)
                .then(function(result){
                    console.log(result);
                    $state.go("PartnershipsComplete");
                })
                .catch(function(error){
                    console.log(error);
                })
        }

        function chooseBidder(){
            PartnershipSvc
                .chooseBidder(vm.selectedBidder,vm.post_id)
                .then(function(result){
                    console.log(result);
                    $state.go("PartnershipsMyPosts", {type:vm.post_data.post_type})
                })
                .catch(function(error){
                    console.log(error);
                })
        }
    }
})();