(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsCompleteCtrl', PartnershipsCompleteCtrl);

    PartnershipsCompleteCtrl.$inject = ["$state", "user"];

    function PartnershipsCompleteCtrl($state, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        // Functions

        // Function declaration and definition

    }
})();