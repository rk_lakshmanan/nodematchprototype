(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsMatchCtrl', PartnershipsMatchCtrl);

    PartnershipsMatchCtrl.$inject = ["user", "$stateParams", "PartnershipSvc"];

    function PartnershipsMatchCtrl(user, $stateParams, PartnershipSvc) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        vm.ps_id = "";
        vm.post_type = "";

        // vm.partners = [];
        vm.companies = [];
        vm.posts = [];

        if ($stateParams.ps_id) {
            console.log('ps_id: ' + $stateParams.ps_id);
            vm.ps_id = $stateParams.ps_id;
            PartnershipSvc
                .matchPartner(vm.ps_id)
                .then(function(result){
                    console.log(result);
                    vm.companies = result.data.company;
                    vm.posts = result.data.post;
                    vm.post_type = vm.posts.length <= 0 ? "" : (vm.posts[0].post_type+"s").toUpperCase();
                })
                .then(function (error){
                    console.log(error);
                })
        }
        // Functions

        // Function declaration and definition

    }
})();