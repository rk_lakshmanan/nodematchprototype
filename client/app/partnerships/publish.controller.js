(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsPublishCtrl', PartnershipsPublishCtrl);

    PartnershipsPublishCtrl.$inject = ["$state", "$stateParams", "user", "PartnershipSvc"];

    function PartnershipsPublishCtrl($state, $stateParams, user, PartnershipSvc) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        vm.ps_id = $stateParams.ps_id;
        vm.publish_options = {
            platform: null,
            settings: null,
        }

        vm.platforms = [
            { value: "public", name: "Public DashBoard" },
            { value: "private", name: "Private Matching" },
        ]
        vm.settings = [
            { value: "1", name: "Publish Anonymously" },
            { value: "2", name: "Publish with Company Info" },
            // {value: "3", name: "Select who can see"},
        ]
        // Functions
        vm.publish = publish;

        // Function declaration and definition
        function publish() {

            console.log("in publish private")
            PartnershipSvc
                .publish(vm.publish_options, vm.ps_id)
                .then((result) => {
                    console.log(result);
                    if (vm.publish_options.platform == "public") {
                        console.log("in publish public")
                        $state.go("PartnershipsComplete");

                    } else if(vm.publish_options.platform == "private") {

                        $state.go("PartnershipsMatch", { ps_id: vm.ps_id })
                    }
                })
                .catch((err) => {
                    console.log(err);

                });

        }
    }

})();