(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsMyPostCtrl', PartnershipsMyPostCtrl);

    PartnershipsMyPostCtrl.$inject = ["$stateParams", "$state", "PartnershipSvc", "user"];

    function PartnershipsMyPostCtrl($stateParams, $state, PartnershipSvc, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        
        vm.post_type = $stateParams.type;
        vm.pending_list = [];

        viewPosts();

        // Functions
        vm.viewPosts = viewPosts;

        // Function declaration and definition
        function viewPosts() {
            PartnershipSvc
                .viewPosts(vm.post_type)
                .then(function(result){
                    console.log(result);
                    vm.pending_list = result.data;
                })
                .catch(function(error){

                })
        }
    }
})();