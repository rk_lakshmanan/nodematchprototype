(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsPostCtrl', PartnershipsPostCtrl);

    PartnershipsPostCtrl.$inject = ["$state", "$stateParams", "PartnershipSvc", "FormOptionSvc", "user"];

    function PartnershipsPostCtrl($state, $stateParams, PartnershipSvc, FormOptionSvc, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.post_type = $stateParams.type[0].toUpperCase() + $stateParams.type.substr(1);
        vm.post_string = ($stateParams.type == "request" ? "Purchase" : "Sales");
        vm.post_option = null;

        vm.post_options = [
            { value: "1", name: "Partnership Opportunities" },
            { value: "2", name: vm.post_string + " of Services only" },
            { value: "3", name: vm.post_string + " of Products only" },
            { value: "4", name: vm.post_string + " of Products & Services" }
        ];

        vm.post_options_1 = {
            post_title: "",
            post_type: $stateParams.type,
            type_of_partnership_interested: [],
            // partner_attributes //
            // track_record_past_performance: "",
            company_size: null,
            type_of_organisation: null,
            // shared_objective: "",
            // branding: "",
            // quality_technical_capability: "",
            // technology_advantage: "",
            // stage_of_technology_development: "",
            financial_capacity: null,
            // intellectual_property: "",
            target_audience: null,
            target_time_frame: null,
            // geography: {
            sector: [],
            country: [],
            state: [],
            // region: "",
            requirement: null,
        };

        vm.formOptions = {
            key_geographical_area: null,
            key_geographical_state: null,
        };

        vm.partnerships = [
            { value: "Joint Ventures" },
            { value: "Mergers/Acquisitions" },
            { value: "Distribution channel/service" },
            { value: "Franchise agreement" },
            { value: "License Agreement" },
            { value: "Manufacturing Agreement" },
            { value: "Outsourcing" },
            { value: "Service Agreement" },
            { value: "Subcontracting" },
            { value: "Funding/Investment Agreement" },
            { value: "Commercial/Sales Agreement " },
            { value: "Development (R&D)" },
            { value: "Marketing and/or Branding" },
        ]

        vm.company_size = [
            { value: "company_1", name: "1-10" },
            { value: "company_2", name: "11-50" },
            { value: "company_3", name: "51-200" },
            { value: "company_4", name: "201-500" },
            { value: "company_5", name: "501-1000" },
            { value: "company_6", name: "1001-10000" },
            { value: "company_7", name: "> 10000" },
            { value: "company_8", name: "NA" },
        ];

        vm.type_of_organisation = [
            { value: "Startup, Entreprenuer", name: "Startup, Entreprenuer" },
            { value: "SME", name: "SME" },
            { value: "Enterprise", name: "Enterprise" },
            { value: "Large Company (< 250 employees)", name: "Large Company (< 250 employees)" },
            { value: "Business Cluster", name: "Business Cluster" },
            { value: "University, Research Centre", name: "University, Research Centre" },
            { value: "Investor (Public/ Private Funds)", name: "Investor (Public/ Private Funds)" },
            { value: "Government Agency/ Institution", name: "Government Agency/ Institution" },
            { value: "Consultant", name: "Consultant" },
            { value: "NA", name: "NA" },
        ];

        vm.financial_capacity = [
            { value: "Below US$50k", name: "Below US$50k" },
            { value: "Between US$50k - US$250k", name: "Between US$50k - US$250k" },
            { value: "Between US$250k - US$500k", name: "Between US$250k - US$500k" },
            { value: "Between US$500k- US$1 million", name: "Between US$500k- US$1 million" },
            { value: "Between US$1 million-US$2million", name: "Between US$1 million-US$2million" },
            { value: "Between US$2 million- US$5million", name: "Between US$2 million- US$5million" },
            { value: "Between US$5 million- US$10million", name: "Between US$5 million- US$10million" },
            { value: "More than US$10 million", name: "More than US$10 million" },
            { value: "NA", name: "NA" },
        ];

        vm.sectors = [
            { value: "Education" },
            { value: "ICT" },
            { value: "Administrative & Support Services" },
            { value: "Wholesale & Retail Trade" },
            { value: "Financial & Insurance Services" },
            { value: "Professional, Scientific & Technical Services" },
            { value: "Health & Social Services" },
            { value: "Accomodation & Food Services" },
        ];

        vm.countries = [];
        vm.states = [];
        // vm.industries = [];
        // vm.influenceDecisions = [
        //     { value: "Final Decision Maker", name: "Final Decision Maker" },
        //     { value: "Key Influence", name: "Key Influence" },
        //     { value: "Initial Recommendations", name: "Initial Recommendations" },
        //     { value: "Research & Analyse new products/solutions", name: "Research & Analyse new products/solutions" },
        //     { value: "Not Applicable", name: "Not Applicable" },
        // ];

        vm.publish_options = {
            platform: null,
            settings: null,
        }

        vm.platforms = [
            { value: "public", name: "Public DashBoard" },
            { value: "private", name: "Private Matching" },
        ]
        vm.settings = [
            { value: "1", name: "Publish Anonymously" },
            { value: "2", name: "Publish with Company Info" },
            // {value: "3", name: "Select who can see"},
        ]

        loadCountries();
        loadStates();

        // Functions
        // vm.addSector = addSector;
        // vm.removeSector = removeSector;
        vm.addArea = addArea;
        vm.removeArea = removeArea;
        vm.matchPartner = matchPartner;
        vm.post = post;
        vm.loadCountries = loadCountries;
        vm.loadStates = loadStates;

        // Function declaration and definition
        function matchPartner() {
            PartnershipSvc
                .matchPartner(vm.form)
                .then(function (result) {

                })
                .catch(function (error) {

                })
        };

        // function addSector() {
        //     vm.form.sectors_interested.push(vm.formOptions.sector_interested);
        // };

        // function removeSector(index) {
        //     vm.form.sectors_interested.splice(index, 1);
        // };

        function addArea() {
            if (vm.formOptions.key_geographical_state) {
                vm.post_options_1.country.push(vm.formOptions.key_geographical_area);
                vm.post_options_1.state.push(vm.formOptions.key_geographical_state);
            }
        };

        function removeArea(index) {
            vm.post_options_1.country.splice(index, 1);
            vm.post_options_1.state.splice(index, 1);
        };

        function post() {
            var storage = [];
            var content = vm.post_options_1.type_of_partnership_interested;
            for (var i = 0; i < content.length; i++) {
                storage.push(content[i].value);
            };
            vm.post_options_1.type_of_partnership_interested = storage;
            storage = [];
            content = vm.post_options_1.sector;
            for (var i = 0; i < content.length; i++) {
                storage.push(content[i].value);
            };
            vm.post_options_1.sector = storage;

            console.log("called")
            PartnershipSvc
                .postPS(vm.post_options_1, vm.publish_options, vm.userid)
                .then((result) => {
                    console.log(result.data.id);
                    if (vm.publish_options.platform == "public" || (vm.publish_options.platform == "private" && vm.publish_options.settings == '4')) {
                        console.log("in publish public")
                        $state.go("PartnershipsComplete");

                    } else if (vm.publish_options.platform == "private" && vm.publish_options.settings == '3') {

                        $state.go("PartnershipsMatch", { ps_id: result.data.id })
                    }
                })
                .catch((err) => {
                    console.log(err);

                });

        };

        function loadCountries() {
            vm.countries = FormOptionSvc.loadCountries();
            // console.log(vm.countries)
        }

        function loadStates() {
            console.log("Getting states");
            FormOptionSvc
                .loadStates(vm.formOptions.key_geographical_area)
                .then(function (result) {
                    console.log(result);
                    vm.states = result.data;
                    vm.formOptions.key_geographical_state = null;
                })
                .catch(function (error) {
                    console.log(error);
                })
        }

    }
})();