(function () {
    angular
        .module('NodeMatch')
        .controller('PartnershipsMainCtrl', PartnershipsMainCtrl);

    PartnershipsMainCtrl.$inject = ["$state", "PartnershipSvc", "user"];

    function PartnershipsMainCtrl($state, PartnershipSvc, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;
        vm.searchString = "";
        vm.filter_options = {
            // category_option: "all",
            category_option: "partner",
            post_option: "all",
            rfx_option: "all",
            pdtsvc_option: "all",
            most_option: "recent"
        }
        vm.category_options = [
            // { value: "all", name: "ALL" },
            { value: "partner", name: "Partnerships" },
            { value: "pdtsvc", name: "Products & Services" },
        ];
        vm.post_options = [
            { value: "all", name: "ALL POSTS" },
            { value: "offer", name: "OFFERS" },
            { value: "request", name: "REQUESTS" },
        ];
        vm.rfx_options = [
            { value: "all", name: "ALL RFx" },
            { value: "RFI", name: "RFI" },
            { value: "REOI", name: "REOI" },
            { value: "RFP", name: "RFP" },
            { value: "RFQ", name: "RFQ" },
        ];
        vm.pdtsvc_options = [
            { value: "all", name: "PRODUCTS & SERVICES" },
            { value: "product", name: "PRODUCTS" },
            { value: "service", name: "SERVICES" },
        ];
        vm.most_options = [
            { value: "recent", name: "RECENT" },
            { value: "relevant", name: "REVELANT" },
            { value: "popular", name: "POPULAR" },
        ];
        vm.page = {
            totalItems: 0,
            currentPage: 0,
            maxSize: 0
        };
        vm.post_list = [];
        vm.pending_requests = 0;
        vm.pending_offers = 0;
        vm.pending_bids = 0;


        getAll();
        viewPendingCount();
        viewBidCount();

        // Functions
        vm.getAll = getAll;
        vm.search = search;
        vm.viewPendingCount = viewPendingCount;
        vm.viewBidCount = viewBidCount;
        vm.viewPost = viewPost;

        // Function declaration and definition
        function getAll() {
            PartnershipSvc
                .getAll()
                .then(function (result) {
                    // console.log(result);
                    for (var i = 0; i < result.data.length; i++) {
                        vm.post_list.push({
                            _id: result.data[i]._id,
                            target_audience: result.data[i].target_audience,
                            target_time_frame: result.data[i].target_time_frame,
                            type_of_organisation: result.data[i].type_of_organisation,
                            requirement: result.data[i].requirement,
                            post_type: result.data[i].post_type,
                            post_title: result.data[i].post_title,
                        })
                    }
                })
                .catch(function (err) {
                    console.log(err);
                })
        };

        function search() {
            if (vm.searchString) {
                PartnershipSvc
                    .search(vm.searchString, vm.filter_options)
                    .then(function (result) {
                        // console.log(result);
                        vm.post_list = [];
                        for (var i = 0; i < result.data.length; i++) {
                            vm.post_list.push({
                                _id: result.data[i]._id,
                                target_audience: result.data[i].target_audience,
                                target_time_frame: result.data[i].target_time_frame,
                                type_of_organisation: result.data[i].type_of_organisation,
                                requirement: result.data[i].requirement,
                                post_type: result.data[i].post_type,
                                post_title: result.data[i].post_title,
                            })
                        }
                    })
                    .catch(function (error) {

                    })
            } else {
                getAll();
            }
        }

        function viewPendingCount() {
            if (vm.userid)
                PartnershipSvc
                    .viewPendingCount()
                    .then(function (result) {
                        // console.log(result);
                        vm.pending_requests = result.data.requestCount;
                        vm.pending_offers = result.data.offerCount;
                    })
                    .catch(function (result) {

                    })
        }

        function viewBidCount() {
            if (vm.userid)
                PartnershipSvc
                    .viewBidCount()
                    .then(function (result) {
                        vm.pending_bids = result.data;
                    })
                    .catch(function (result) {

                    })
        }

        function viewPost(id) {
            if (vm.userid) {
                console.log("True");
                $state.go('PartnershipsOnePost', { id: id, view: 'match' })
            } else {
                console.log("False")
            }
        }
    }
})();