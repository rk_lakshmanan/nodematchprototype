(function () {
    angular
        .module("NodeMatch")
        .service("UserService", UserService);

    UserService.$inject = ['$http'];

    function UserService($http) {

        var service = this;

        // Data models ------------------------------------------------------------------------

        // Functions --------------------------------------------------------------------------
        service.userAuth = userAuth;
        service.logout = logout;
        service.login = login;
        service.signUp = signUp;
        service.resetPw = resetPw;
        service.getProfileContact = getProfileContact;
        service.editProfileContact = editProfileContact;
        service.getCompanyProfile = getCompanyProfile;
        service.editCompanyProfile = editCompanyProfile;
        service.notifications = notifications;
        service.notified = notified;
        service.getUserList = getUserList;
        service.deleteUser = deleteUser;

        // Function declaration and definition --------------------------------------------------------
        function userAuth() {
            return $http.get(
                '/user/auth',
            );
        }

        function logout() {
            return $http.get(
                '/user/logout'
            )
        }

        function login(credentials) {
            // console.log("Verifying Credentials");
            return $http({
                method: "POST",
                url: "user/login",
                data: {
                    // credentials: credentials
                    username: credentials.username,
                    password: credentials.password,
                }
            })
        };

        function signUp(details) {
            return $http({
                method: "POST",
                url: "user/signUp",
                data: {
                    details: details
                }
            })
        };

        function resetPw(email) {
            return $http({
                method: "PUT",
                url: "user/resetPw",
                data: {
                    email: email
                }
            })
        };

        function getProfileContact(id) {
            return $http({
                method: "GET",
                url: "user/profile/contact/" + id,
            })
        };

        function editProfileContact(id, details) {
            return $http({
                method: "PUT",
                url: "user/profile/contact/" + id,
                data: {
                    details: details
                }
            })
        };

        function getCompanyProfile(id) {
            console.log("serv Company profile")
            return $http({
                method: "GET",
                url: "user/profile/company",
                params: {
                    id: id
                }
            })
        };

        function editCompanyProfile(id,details) {
            console.log("serv edit")
            return $http({
                method: "PUT",
                url: "user/profile/company",
                data: {
                    details: details,
                    userid:id,
                }

            })
        };

        function notifications(){
            // console.log("get notified num")
            return $http({
                method: "GET",
                url: "/ps/notify"
            })
        }

        function notified(){
            // console.log("notified");
            return $http({
                method: "GET",
                url: "/ps/notifications"
            })
        }

        function getUserList(search){
            return $http({
                method: "GET",
                url: "admin/users",
                params: {
                    search: search
                }
            })
        };

        function deleteUser(id){
            return $http({
                method: "DELETE",
                url: "user/" + id,
            })
        };

    }
})();
