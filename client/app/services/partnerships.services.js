(function () {
    angular
        .module("NodeMatch")
        .service("PartnershipSvc", PartnershipSvc);

    PartnershipSvc.$inject = ['$http'];

    function PartnershipSvc($http) {

        var service = this;

        // Data models ------------------------------------------------------------------------

        // Functions --------------------------------------------------------------------------
        service.search = search;
        service.viewPendingCount = viewPendingCount;
        service.viewBidCount = viewBidCount;
        service.viewPosts = viewPosts;
        service.viewOnePost = viewOnePost;
        service.acceptPost = acceptPost;
        service.chooseBidder = chooseBidder;
        service.mybids = mybids;
        service.matchPartner = matchPartner;
        service.postPS = postPS;
        service.publish = publish;
        service.getAll = getAll;


        // Function declaration and definition --------------------------------------------------------
        function search(string, filter) {
            console.log('search')
            return $http({
                method: "GET",
                url: "/ps/search",
                params: {
                    string: string,
                    filter: filter
                },
            })
        };

        function viewPendingCount(){
            // console.log('getting pending count');
            return $http({
                method: "GET",
                url: "/ps/pending"
            })
        }

        function viewBidCount(){
            return $http({
                method: "GET",
                url: "/ps/pending/bids"
            })
        }

        function viewPosts(type){
            console.log('viewing posts');
            return $http({
                method: "GET",
                url: "/ps/post",
                params: {
                    post_type: type
                }
            })
        }

        function viewOnePost(id){
            console.log('one posts');
            return $http({
                method: "GET",
                url: "/ps/post/" + id
            })
        }

        function acceptPost(id){
            console.log('accepting');
            return $http({
                method: "PUT",
                url: "/ps/bid/" + id
            })
        }

        function chooseBidder(id,postid){
            console.log('This bidder');
            return $http({
                method: "PUT",
                url: "/ps/accept/" + postid,
                data:{
                    bidder_id : id
                }
            })
        }

        function mybids(){
            return $http({
                method: "GET",
                url: "/ps/mybids"
            })
        }

        function matchPartner(id) {
            console.log("match")
            return $http({
                method: "GET",
                url: "/ps/match/" + id,
                // data: {
                // form: form
                // }
            })
        };

        function postPS(form, platform, userid) {
            console.log("postPS")
            return $http({
                method: "POST",
                url: "/ps/",
                data: {
                    form: form,
                    platform: platform,
                    userid: userid,
                }
            })
        };

        function publish(ps_options, ps_id) {
            console.log("publish service")
            return $http({
                method: "PUT",
                url: "/ps/" + ps_id,
                data: {
                    details: ps_options,
                }
            })
        };

        function getAll() {
            console.log("Load all public post");
            return $http({
                method: "GET",
                url: "ps/public"
            })
        };

    }
})();
