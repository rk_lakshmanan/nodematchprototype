(function () {
    angular
        .module('NodeMatch')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ["$state", "UserService", "$http", "$analytics"];

    function LoginCtrl($state, UserService, $http, $analytics) {

        var vm = this;
        $analytics.eventTrack('login');
        // Data models
        // vm.user = {
        //     username: "",
        //     password: ""
        // }

        vm.user = {
            username: "ben@ben.ben",
            password: "123456aA@"
        };

        vm.message = false;

        // Functions
        vm.login = login;

        // Function declaration and definition
        function login() {
            // console.log(vm.user.username)
            // console.log(vm.user.password)
            UserService
                .login(vm.user)
                .then(function (result) {
                    if (result.status == 200) {
                        getUserStatus((userResult) => {
                            if (userResult) {
                                console.log(userResult);
                                // console.log("userResult")
                                UserService.userAuth()
                                    .then(function (result) {
                                        if (result.data.privilege == "admin") {
                                            $state.go("Admin");
                                        } else {
                                            $state.go("Partnerships");
                                        }
                                    })
                            } else {
                                console.log("user not registered")
                                //  deferred.reject();
                                // Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                                // $state.go('SignIn');
                                vm.message = true;
                            }
                        })
                    }
                    // console.log("successful login");
                    // $state.go("Partnerships");
                })
                .catch(function (err) {
                    console.log(err);
                    console.log("access denied");
                });
        }
        function getUserStatus(callback) {
            $http.get('/status/user')
                // handle success
                .then(function (data) {
                    var authResult = JSON.stringify(data);
                    if (data["data"] != '') {
                        user = true;
                        callback(user);
                    } else {
                        user = false;
                        callback(user);
                    }
                });
        }

    }
})();
