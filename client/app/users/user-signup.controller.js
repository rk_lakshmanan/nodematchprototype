(function () {
    angular
        .module('NodeMatch')
        .controller('SignUpCtrl', SignUpCtrl);

    SignUpCtrl.$inject = ["$state", "UserService"];

    function SignUpCtrl($state, UserService) {

        var vm = this;

        // Data models
        // vm.user = {
        //     salutation: null,
        //     first_name: "",
        //     last_name: "",
        //     username: "", //email address
        //     password: "",
        //     confirm_password: "",
        //     company_name: "",
        // };

        vm.user = {
            salutation: "Mr",
            first_name: "ben",
            last_name: "ben",
            username: "ben@ben.ben", //email address
            password: "123456aA@",
            confirm_password: "123456aA@",
            company_name: "123456aA@",
        };

        vm.salutations = [
            { value: "Mr", name: "Mr." },
            { value: "Mrs.", name: "Mrs." },
            { value: "Ms.", name: "Ms." }
        ];

        vm.passwordFeedback = "";
        vm.passwordValid = false;

        // Functions
        vm.signUp = signUp;
        vm.checkPassword = checkPassword;
        vm.validEmail = validEmail;

        // Function declaration and definition
        function signUp() {
            console.log(vm.user);
            UserService
                .signUp(vm.user)
                .then((result) => {
                    console.log(result);
                    $state.go("Login");
                })
                .catch((err) => {
                    console.log(err);
                })
        }

        function checkPassword() {
            if(vm.user.password == undefined){
                return false;
            }
            var feedback = "";
            var pass = true;
            if(vm.user.password.length){
                if (vm.user.password.length < 8) {
                    feedback += "Too short, ";
                    pass = false;
                }
                if (vm.user.password.search(/[0-9]/) == -1) {
                    feedback += "No number, ";
                    pass = false;
                }
                if (vm.user.password.search(/[a-z]/) == -1) {
                    feedback += "No lowercase letter, ";
                    pass = false;
                }
                if (vm.user.password.search(/[A-Z]/) == -1) {
                    feedback += "No uppercase letter, ";
                    pass = false;
                }
                if (vm.user.password.search(/[@#$]/) == -1) {
                    feedback += "No special character @ # $, ";
                    pass = false;
                }
                vm.passwordFeedback = feedback.substring(0,feedback.length-2);
                vm.passwordValid = pass;
                return pass;
            }
        }

        function validEmail(){
            // TODO: check database for duplicate email
        }
    }
})();
