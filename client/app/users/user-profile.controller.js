(function () {
    angular
        .module('NodeMatch')
        .controller('ProfileCtrl', ProfileCtrl);

    ProfileCtrl.$inject = ["user"];

    function ProfileCtrl(user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        // Functions

        // Function declaration and definition

    }
})();
