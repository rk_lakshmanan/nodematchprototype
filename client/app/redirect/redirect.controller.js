(function () {
    angular
        .module('NodeMatch')
        .controller('RedirectCtrl', RedirectCtrl);

    RedirectCtrl.$inject = ["user"];

    function RedirectCtrl(user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        // Functions

        // Function declaration and definition

    }
})();
