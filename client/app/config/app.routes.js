(function () {
    angular
        .module("NodeMatch")
        .config(uiRoutesConfig);

    uiRoutesConfig.$inject = ["$stateProvider", "$urlRouterProvider","$analyticsProvider"];

    function uiRoutesConfig($stateProvider, $urlRouterProvider,$analyticsProvider) {
        $stateProvider
            .state("Home", {
                url: "/home",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/home/home.html",
                        controller: "HomeCtrl",
                        controllerAs: "ctrl"
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Login", {
                url: "/login",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/users/user-login.html",
                        controller: 'LoginCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html"
                    },
                    "content": {
                        templateUrl: "/app/users/user-signup.html",
                        controller: 'SignUpCtrl',
                        controllerAs: 'ctrl'
                    }
                }
            })
            .state("ResetPassword", {
                url: "/resetPassword",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html"
                    },
                    "content": {
                        templateUrl: "/app/users/user-reset-password.html",
                        controller: 'ResetPWCtrl',
                        controllerAs: 'ctrl'
                    }
                }
            })
            .state("Profile", {
                url: "/profile",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/users/user-profile.html",
                        controller: 'ProfileCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Contact", {
                url: "/profile/contact",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/profiles/contact.html",
                        controller: 'ContactCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Company", {
                url: "/profile/company",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/profiles/company.html",
                        controller: 'CompanyCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Partnerships", {
                url: "/partnerships",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/main.html",
                        controller: 'PartnershipsMainCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsPost", {
                url: "/partnerships/post/:type",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/post.html",
                        controller: 'PartnershipsPostCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsMatch", {
                url: "/partnerships/match/:ps_id",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/match.html",
                        controller: 'PartnershipsMatchCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsPublish", {
                url: "/partnerships/publish/:ps_id",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/publish.html",
                        controller: 'PartnershipsPublishCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsComplete", {
                url: "/partnerships/complete",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/complete.html",
                        controller: 'PartnershipsCompleteCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsMyPosts", {
                url: "/partnerships/view/:type",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/myposts.html",
                        controller: 'PartnershipsMyPostCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsOnePost", {
                url: "/partnerships/view/:view/post/:id",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/viewpost.html",
                        controller: 'PartnershipsOnePostCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("PartnershipsMyBids", {
                url: "/partnerships/bids",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/partnerships/mybids.html",
                        controller: 'PartnershipsMyBidsCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFX", {
                url: "/rfx",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx.html",
                        controller: 'RfxCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFXPost", {
                url: "/rfx/post",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx-post.html"
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFXPostRFQ", {
                url: "/rfx/post/rfq",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx-post-rfq.html",
                        controller: 'RfxPostRfqCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFXPostRFQReview", {
                url: "/rfx/post/rfq/review",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx-post-rfq-review.html",
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFXPostRFQSubmit", {
                url: "/rfx/post/rfq/submit",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx-post-rfq-submit.html",
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("RFXView", {
                url: "/rfx/view",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/rfx/rfx-view.html",
                        controller: 'RfxViewCtrl',
                        controllerAs: 'ctrl'
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Mall", {
                url: "/mall",
                views: {
                    "nav": {
                        templateUrl: "/app/navigation/navigation.html",
                        controller: 'NavCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "../assets/messages/comingsoon.html"
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("Admin", {
                url: "/admin",
                views: {
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/admin/admin.html",
                        controller: "AdminCtrl",
                        controllerAs: "ctrl"
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })
            .state("AdminEdit", {
                url: "/admin/edit/:id",
                views: {
                    "redirect": {
                        templateUrl: "/app/redirect/redirect.html",
                        controller: 'RedirectCtrl',
                        controllerAs: 'ctrl'
                    },
                    "content": {
                        templateUrl: "/app/admin/admin-edit.html",
                        controller: "AdminEditCtrl",
                        controllerAs: "ctrl"
                    }
                },
                resolve: {
                    user: function (UserService) {
                        return UserService.userAuth()
                            .then(function (result) {
                                return result.data;
                            })
                            .catch(function (err) {
                                return '';
                            });
                    }
                },
            })

        $urlRouterProvider.otherwise("/home");
    }
})();