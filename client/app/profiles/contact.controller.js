(function () {
    angular
        .module('NodeMatch')
        .controller('ContactCtrl', ContactCtrl);

    ContactCtrl.$inject = ["$state", "UserService", "user"];

    function ContactCtrl($state, UserService, user) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        vm.user = {
            salutation: null,
            first_name: "",
            last_name: "",
            username: "", //email address
            contact_number: "",
            fax_number: "",
            department: null,
            department_others: "",
            function: null,
            function_others: "",
            purpose: null,
        };

        // vm.user = {
        //     salutation: "Mr",
        //     first_name: "ben",
        //     last_name: "ben",
        //     username: "ben@ben.ben", //email address
        //     contact_number: "12345678",
        //     fax_number: "12345678",
        //     department: "Management",
        //     department_others: "",
        //     function: "Developer",
        //     function_others: "",
        //     purpose: "Networking",
        // };

        vm.salutations = [
            { value: "Mr", name: "Mr." },
            { value: "Mrs.", name: "Mrs." },
            { value: "Ms.", name: "Ms." }
        ];

        vm.departments = [
            { value: "Administration", name: "Administration" },
            { value: "Management", name: "Management" },
            { value: "Finance/Accounting", name: "Finance/Accounting" },
            { value: "Human Resources", name: "Human Resources" },
            { value: "Information Technology", name: "Information Technology" },
            { value: "Purchasing", name: "Purchasing" },
            { value: "Marketing", name: "Marketing" },
            { value: "Operations/ Logistics", name: "Operations/ Logistics" },
            { value: "Materials Management", name: "Materials Management" },
            { value: "Legal", name: "Legal" },
            { value: "Ecommerce", name: "Ecommerce" },
            { value: "Sales", name: "Sales" },
            { value: "Treasury", name: "Treasury" },
            { value: "Consulting", name: "Consulting" },
            { value: "Others", name: "Others" },
        ];

        vm.functions = [
            { value: "Company Owner", name: "Company Owner" },
            { value: "Chief Executive Officer", name: "Chief Executive Officer" },
            { value: "Chief Operating Officer", name: "Chief Operating Officer" },
            { value: "Chief Financial Officer", name: "Chief Financial Officer" },
            { value: "Chief Marketing Officer", name: "Chief Marketing Officer" },
            { value: "Managing Director/ President/ Vice President", name: "Managing Director/ President/ Vice President" },
            { value: "Department Head/ Director", name: "Department Head/ Director" },
            { value: "Manager/ Team Lead/ Supervisor", name: "Manager/ Team Lead/ Supervisor" },
            { value: "Developer", name: "Developer" },
            { value: "Assistant/ Support Officer", name: "Assistant/ Support Officer" },
            { value: "Others", name: "Others" },
        ];

        vm.purpose = [
            { value: "Looking for OEM", name: "Looking for OEM" },
            { value: "Looking for Product Suppliers", name: "Looking for Product Suppliers" },
            { value: "Looking for Services", name: "Looking for Services" },
            { value: "Looking for Component", name: "Looking for Component" },
            { value: "Networking", name: "Networking" },
            { value: "Collect Market information and trends", name: "Collect Market information and trends" },
            { value: "Seek Potential Clients", name: "Seek Potential Clients" },
            { value: "Seek Franchises/Principals", name: "Seek Franchises/Principals" },
            { value: "Attend events, seminars, workshops", name: "Attend events, seminars, workshops" },
        ];

        // Functions
        vm.getProfileContact = getProfileContact;
        vm.editProfileContact = editProfileContact;

        // Initialise
        getProfileContact();

        // Function declaration and definition
        function getProfileContact() {
            UserService
                .getProfileContact(vm.userid)
                .then(function (result) {
                    console.log(result)
                    result = result.data;
                    var details = {
                        salutation: result.salutation,
                        first_name: result.first_name,
                        last_name: result.last_name,
                        username: result.username, //email address
                        contact_number: result.contact_number,
                        fax_number: result.fax_number,
                        department: result.department,
                        department_others: result.department_others,
                        function: result.function,
                        function_others: result.function_others,
                        purpose: result.purpose,
                    };
                    vm.user = details;
                })
                .catch(function (error) {
                    console.log(error);

                });
        };

        function editProfileContact() {
            UserService
                .editProfileContact(vm.userid, vm.user)
                .then(function (result) {
                    console.log(result);
                    $state.go("Profile");
                })
                .catch(function (error) {
                    console.log(result);
                });
        };

    }
})();
