(function () {
    angular
        .module('NodeMatch')
        .controller('CompanyCtrl', CompanyCtrl);

    CompanyCtrl.$inject = ["UserService", "FormOptionSvc", "user", "$state"];

    function CompanyCtrl(UserService, FormOptionSvc, user, $state) {

        var vm = this;

        // Data models
        vm.username = user.username;
        vm.userid = user._id;
        vm.privilege = user.privilege;

        vm.company = {
            company_name: "",
            location_country: null,
            location_state: null,
            location_city: "",
            location_street: "",
            location_zip: "",
            company_contact: "",
            company_crn: "",
            type_of_organisation: null,
            company_size: null,
            main_activity: "",
            company_coverage: [],
            company_coverage_s: [],
            estimated_revenue_sales: null,
            sectors: [],
            products_services: [],
            past_orders: "",
            // company_certifications: [],
        };

        vm.form_options = {
            coverage_area: null,
            coverage_state: null,
            product_service: ""
        };

        vm.company_size = [
            { value: "company_1", name: "1-10" },
            { value: "company_2", name: "11-50" },
            { value: "company_3", name: "51-200" },
            { value: "company_4", name: "201-500" },
            { value: "company_5", name: "501-1000" },
            { value: "company_6", name: "1001-10000" },
            { value: "company_7", name: "> 10000" },
        ];

        vm.type_of_organisation = [
            { value: "Startup, Entreprenuer", name: "Startup, Entreprenuer" },
            { value: "SME", name: "SME" },
            { value: "Enterprise", name: "Enterprise" },
            { value: "Large Company (< 250 employees)", name: "Large Company (< 250 employees)" },
            { value: "Business Cluster", name: "Business Cluster" },
            { value: "University, Research Centre", name: "University, Research Centre" },
            { value: "Investor (Public/ Private Funds)", name: "Investor (Public/ Private Funds)" },
            { value: "Government Agency/ Institution", name: "Government Agency/ Institution" },
            { value: "Consultant", name: "Consultant" },
            { value: "NA", name: "NA" },
        ];

        vm.financial_capacity = [
            { value: "Below US$50k", name: "Below US$50k" },
            { value: "Between US$50k - US$250k", name: "Between US$50k - US$250k" },
            { value: "Between US$250k - US$500k", name: "Between US$250k - US$500k" },
            { value: "Between US$500k- US$1 million", name: "Between US$500k- US$1 million" },
            { value: "Between US$1 million-US$2million", name: "Between US$1 million-US$2million" },
            { value: "Between US$2 million- US$5million", name: "Between US$2 million- US$5million" },
            { value: "Between US$5 million- US$10million", name: "Between US$5 million- US$10million" },
            { value: "More than US$10 million", name: "More than US$10 million" },
            { value: "NA", name: "NA" },
        ];

        vm.sectors = [
            { value: "Education", ticked: false },
            { value: "ICT", ticked: false },
            { value: "Administrative & Support Services", ticked: false },
            { value: "Wholesale & Retail Trade", ticked: false },
            { value: "Financial & Insurance Services", ticked: false },
            { value: "Professional, Scientific & Technical Services", ticked: false },
            { value: "Health & Social Services", ticked: false },
            { value: "Accomodation & Food Services", ticked: false },
        ];

        vm.countries = [];
        vm.statesI = [];
        vm.statesO = [];

        // Functions
        vm.addCoverage = addCoverage;
        vm.removeCoverage = removeCoverage;
        vm.addProductService = addProductService;
        vm.removeProductService = removeProductService;
        vm.editCompanyProfile = editCompanyProfile;
        vm.getCompanyProfile = getCompanyProfile;
        vm.loadCountries = loadCountries;
        vm.loadStates = loadStates;
        vm.loadStatesI = loadStatesI;
        vm.loadStatesO = loadStatesO;

        loadCountries();
        loadStatesO();
        getCompanyProfile();

        // Function declaration and definition
        function getCompanyProfile() {
            console.log("get Company profile")
            console.log(vm.userid);
            UserService
                .getCompanyProfile(vm.userid)
                .then((result) => {
                    console.log(result);
                    // result.data;
                    var storage = [];
                    var content = [];
                    vm.company.company_name = result.data.company_name;
                    vm.company.location_country = result.data.location_country;
                    vm.company.location_state = result.data.location_state;
                    vm.company.location_city = result.data.location_city;
                    vm.company.location_street = result.data.location_street;
                    vm.company.location_zip = result.data.location_zip;
                    vm.company.company_contact = result.data.company_contact;
                    vm.company.company_crn = result.data.company_crn;
                    vm.company.type_of_organisation = result.data.type_of_organisation;
                    vm.company.company_size = result.data.company_size;
                    vm.company.main_activity = result.data.main_activity;
                    vm.company.company_coverage = result.data.company_coverage ? result.data.company_coverage : [];
                    vm.company.company_coverage_s = result.data.company_coverage_s ? result.data.company_coverage_s : [];
                    vm.company.estimated_revenue_sales = result.data.estimated_revenue_sales;
                    content = result.data.sectors == null ? [] : (function () {

                        for (var i = 0; i < result.data.sectors.length; i++) {
                            for (var j = 0; j < vm.sectors.length; j++) {
                                if (vm.sectors[j].value == result.data.sectors[i]) {
                                    vm.sectors[j].ticked = true;
                                    break;
                                }
                            }
                        };
                    })();
                    vm.company.sectors = content;
                    vm.company.products_services = result.data.products_services;
                    vm.company.past_orders = result.data.past_orders;
                    loadStates();
                })
                .catch((err) => {
                    console.log(err);

                });

        }

        function addCoverage() {
            if (vm.form_options.coverage_area) {
                vm.company.company_coverage.push(vm.form_options.coverage_area);
                vm.company.company_coverage_s.push(vm.form_options.coverage_state);
            }
        };

        function removeCoverage(index) {
            vm.company.company_coverage.splice(index, 1);
            vm.company.company_coverage_s.splice(index, 1);
        };

        function addProductService() {
            if (vm.form_options.product_service) {
                vm.company.products_services.push(vm.form_options.product_service);
            }
        };

        function removeProductService(index) {
            vm.company.products_services.splice(index, 1);
        };

        function editCompanyProfile() {
            console.log("save");
            var storage = [];
            var content = vm.company.sectors;
            for (var i = 0; i < content.length; i++) {
                storage.push(content[i].value);
            };
            vm.company.sectors = storage;
            UserService
                .editCompanyProfile(vm.userid, vm.company)
                .then((result) => {
                    console.log(result);
                    $state.go("Profile")
                })
                .catch((err) => {
                    console.log(err);

                });

        }

        function loadCountries() {
            vm.countries = FormOptionSvc.loadCountries();
            // console.log(vm.countries)
        }

        function loadStates() {
            console.log("Getting states");
            FormOptionSvc
                .loadStates(vm.company.location_country)
                .then(function (result) {
                    console.log(result);
                    vm.statesI = result.data;
                })
                .catch(function (error) {
                    console.log(error);
                })
        }

        function loadStatesI() {
            console.log("Getting states");
            FormOptionSvc
                .loadStates(vm.company.location_country)
                .then(function (result) {
                    console.log(result);
                    vm.statesI = result.data;
                    vm.company.location_state = null;
                })
                .catch(function (error) {
                    console.log(error);
                })
        }

        function loadStatesO() {
            console.log("Getting states");
            FormOptionSvc
                .loadStates(vm.form_options.coverage_area)
                .then(function (result) {
                    console.log(result);
                    vm.statesO = result.data;
                    vm.form_options.coverage_state = null;
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    }
})();
